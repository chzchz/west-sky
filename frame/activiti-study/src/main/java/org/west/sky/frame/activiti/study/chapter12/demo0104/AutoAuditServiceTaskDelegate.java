package org.west.sky.frame.activiti.study.chapter12.demo0104;

import org.activiti.engine.delegate.BpmnError;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

/**
 * author: chz
 * date: 2025/3/5
 * description:
 */
public class AutoAuditServiceTaskDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("====开始自动审核");
        String state = execution.getVariable("state").toString();
        if ("false".equals(state)) {
            throw new BpmnError("auditError");
        }
    }
}
