package org.west.sky.frame.activiti.study.chapter8;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;

/**
 * author: chz
 * date: 2024/9/6
 * description: 流程部署
 */
public class RunDemo8_1 {

    public static void main(String[] args) {
        ProcessEngineConfiguration configuration = ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti.cfg.xml");
        ProcessEngine processEngine = configuration.buildProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        Deployment deploy = repositoryService.createDeployment().name("部署1").category("类目1").key("key1").addClasspathResource("processes/chapter8/Chapter8_1.bpmn20.xml").deploy();

        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
        System.out.println("流程部署ID:" + deploy.getId() + ",流程名称:" + processDefinition.getName() + ",流程定义ID:" + processDefinition.getId()
                + ",流程版本：" + processDefinition.getVersion());

        Deployment deploy2 = repositoryService.createDeployment().name("部署2").category("类目1").key("key1").addClasspathResource("processes/chapter8/Chapter8_1.bpmn20.xml").deploy();

        ProcessDefinition processDefinition2 = repositoryService.createProcessDefinitionQuery().deploymentId(deploy2.getId()).singleResult();
        System.out.println("流程部署ID:" + deploy2.getId() + ",流程名称:" + processDefinition2.getName() + ",流程定义ID:" + processDefinition2.getId()
                + ",流程版本：" + processDefinition2.getVersion());
        processEngine.close();


    }
}
