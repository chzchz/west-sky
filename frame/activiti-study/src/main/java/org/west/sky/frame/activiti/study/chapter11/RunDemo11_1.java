package org.west.sky.frame.activiti.study.chapter11;

import lombok.SneakyThrows;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

/**
 * author: chz
 * date: 2025/2/20
 * description: 定时开始事件
 */
public class RunDemo11_1 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo11_1 runDemo11_1 = new RunDemo11_1();
        runDemo11_1.deploy();
    }

    @SneakyThrows
    public void deploy(){
        loadActivitiConfigAndInitEngine("activiti.job.xml");
        Deployment deploy = repositoryService.createDeployment().name("定时开始事件").addClasspathResource("processes/chapter11/Chapter11_1.bpmn20.xml")
                .deploy();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
        Thread.sleep(10000);
        Task task = taskService.createTaskQuery().processDefinitionId(processDefinition.getId()).singleResult();
        System.out.println("第一个任务id:" + task.getId() + "，任务名称:" + task.getName());
        taskService.complete(task.getId());
        closeEngine();
    }
}
