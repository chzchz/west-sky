package org.west.sky.frame.activiti.study;

import org.activiti.spring.boot.SecurityAutoConfiguration;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.assertj.core.util.DateUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * author: chz
 * date: 2024/2/24
 * description:
 */
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
//@EnableScheduling
//@EnableAsync
public class ActivitiStudyApplication {
    public static void main(String[] args) {
        SpringApplication.run(ActivitiStudyApplication.class, args);
    }

//    @Scheduled(cron = "0/2 * * * * ? ")
//    @Async
    public void aa() throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + ":1111111");
    }


//    @Scheduled(cron = "0/2 * * * * ? ")
//    @Async
    public void bb() throws InterruptedException {
        Thread.sleep(10000);
        System.out.println(Thread.currentThread().getName() + ":2222222");
    }

//    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(5);
        return taskScheduler;
    }

//    @Bean(name = "scheduledExecutorService")
    protected ScheduledExecutorService scheduledExecutorService() {
        return new ScheduledThreadPoolExecutor(5,
                new BasicThreadFactory.Builder().namingPattern("schedule-pool-%d").daemon(true).build(),
                new ThreadPoolExecutor.CallerRunsPolicy()) {
//            @Override
//            protected void afterExecute(Runnable r, Throwable t) {
//                super.afterExecute(r, t);
//                Threads.printException(r, t);
//            }
        };
    }
}
