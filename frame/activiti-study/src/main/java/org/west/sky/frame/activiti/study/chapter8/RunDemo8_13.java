package org.west.sky.frame.activiti.study.chapter8;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

import java.util.List;

/**
 * author: chz
 * date: 2024/9/6
 * description: 身份服务
 */
public class RunDemo8_13 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo8_13 runDemo8_3 = new RunDemo8_13();
        runDemo8_3.runDemo();
    }

    private void runDemo() {
        loadActivitiConfigAndInitEngine("activiti.cfg.xml");

        //创建用户
        User zs = identityService.newUser("zs");
        zs.setFirstName("张");
        zs.setLastName("三");
        zs.setEmail("zhangsan@163.com");
        identityService.saveUser(zs);

        User ls = identityService.newUser("ls");
        ls.setFirstName("李");
        ls.setLastName("四");
        ls.setEmail("lisi@163.com");
        identityService.saveUser(ls);

        User ww = identityService.newUser("ww");
        ww.setFirstName("王");
        ww.setLastName("五");
        ww.setEmail("wangwu@163.com");
        identityService.saveUser(ww);

        //创建用户组
        Group group = identityService.newGroup("g1");
        group.setName("组1");
        identityService.saveGroup(group);

        Group group2 = identityService.newGroup("g2");
        group2.setName("组2");
        identityService.saveGroup(group2);

        //绑定用户组关系
        identityService.createMembership("zs", "g1");
        identityService.createMembership("ls", "g1");
        identityService.createMembership("ww", "g2");

        //查询绑定关系
        List<Group> groups = identityService.createGroupQuery().list();
        for (Group g : groups) {
            List<User> list = identityService.createUserQuery().memberOfGroup(g.getId()).list();
            list.forEach(e -> System.out.println(g.getName() + "包括成员:" + e.getFirstName() + e.getLastName()));
        }
    }

}
