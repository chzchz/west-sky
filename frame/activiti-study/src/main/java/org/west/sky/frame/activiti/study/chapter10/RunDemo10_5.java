package org.west.sky.frame.activiti.study.chapter10;

import org.activiti.engine.impl.persistence.entity.DeploymentEntity;
import org.activiti.engine.impl.persistence.entity.ResourceEntity;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

import java.util.Map;

/**
 * author: chz
 * date: 2025/1/20
 * description: 完整部署流程
 */
public class RunDemo10_5 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo10_5 runDemo10_2 = new RunDemo10_5();
        runDemo10_2.deploy();
    }

    public void deploy() {
        loadActivitiConfigAndInitEngine();
        Deployment deployment = repositoryService.createDeployment()
                .name("请假流程部署").category("请假").key("holiday").tenantId("zs")
                .addClasspathResource("processes/LeaveApplyProcess.bpmn20.xml")
                .deploy();
        System.out.println("流程部署ID:" + deployment.getId() + ",流程名称:" + deployment.getName());
        if (deployment instanceof DeploymentEntity) {
            DeploymentEntity deploymentEntity = (DeploymentEntity) deployment;
            Map<String, ResourceEntity> resources = deploymentEntity.getResources();
            for (ResourceEntity resourceEntity : resources.values()) {
                System.out.println("资源名称：" + resourceEntity.getName());
            }
        }
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey("LeaveApplyProcess").active().latestVersion().singleResult();
        System.out.println("流程部署ID:" + processDefinition.getDeploymentId() + ",流程名称:" + processDefinition.getName() + ",流程定义ID:" + processDefinition.getId()
                + ",流程版本：" + processDefinition.getVersion());
        processEngine.close();
    }
}
