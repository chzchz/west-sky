package org.west.sky.frame.activiti.study.chapter13;

import lombok.SneakyThrows;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.west.sky.frame.activiti.study.chapter13.demo0104.TaskAssigneeBean;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author: chz
 * date: 2025/3/12
 * description: 动态分配用户任务_表达式配置
 */
public class RunDemo13_1_4 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo13_1_4 runDemo13_1_1 = new RunDemo13_1_4();
        runDemo13_1_1.deploy();
    }

    @SneakyThrows
    private void deploy() {
        loadActivitiConfigAndInitEngine("activiti.cfg.xml");
        //部署流程
//        ProcessDefinition processDefinition = deployByClassPathResource("processes/chapter13/Chapter13_1_4_1.bpmn20.xml");
        ProcessDefinition processDefinition = deployByClassPathResource("processes/chapter13/Chapter13_1_4_2.bpmn20.xml");
        //开启流程实例
        TaskAssigneeBean taskAssigneeBean = new TaskAssigneeBean();
        taskAssigneeBean.setApprover("approver");
        Map<String, Object> variables = new HashMap<>();
        variables.put("taskAssigneeBean", taskAssigneeBean);
        ProcessInstance processInstance = runtimeService.startProcessInstanceById(processDefinition.getId(), variables);
        //完成任务
        Task task_1 = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("====当前流程：" + processInstance.getId() + "进行到：" + task_1.getName() + "：任务节点,办理人是：" + task_1.getAssignee());
        taskService.complete(task_1.getId());

        Task task_2 = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("====当前流程：" + processInstance.getId() + "进行到：" + task_2.getName() + "：任务节点");
        List<IdentityLink> links = taskService.getIdentityLinksForTask(task_2.getId());
        for (IdentityLink link : links) {
            System.out.println("====办理人候选人是：" + link.getUserId() + "，类型是：" + link.getType());
        }
        taskService.claim(task_2.getId(), "manager1");
        taskService.complete(task_2.getId());


        Task task_3 = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("====当前流程：" + processInstance.getId() + "进行到：" + task_3.getName() + "：任务节点");
        List<IdentityLink> links1 = taskService.getIdentityLinksForTask(task_3.getId());
        for (IdentityLink link : links1) {
            System.out.println("====办理人候选组是：" + link.getGroupId() + "，类型是：" + link.getType());
        }
        taskService.claim(task_3.getId(), "user1");
        taskService.complete(task_3.getId());

        Task task_4 = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("====当前流程：" + processInstance.getId() + "进行到：" + task_4.getName() + "：任务节点,办理人是：" + task_4.getAssignee());
        taskService.complete(task_4.getId());
        closeEngine();
    }
}
