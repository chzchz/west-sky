package org.west.sky.frame.activiti.study.chapter7;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.*;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * author: chz
 * date: 2024/9/4
 * description:
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
public class RunFirstDemoTest {
    @Test
    public void demo1() {
        //创建默认工作流引擎，读取activiti.cfg.xml配置初始化引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        //获取工作流引擎的服务组件
        RepositoryService repositoryService = processEngine.getRepositoryService();
        RuntimeService runtimeService = processEngine.getRuntimeService();
        TaskService taskService = processEngine.getTaskService();
        //部署流程
        repositoryService.createDeployment().addClasspathResource("processes/LeaveApplyProcess.bpmn20.xml").deploy();
        //启动流程实例
        runtimeService.startProcessInstanceByKey("LeaveApplyProcess");
        //查询任务1
        Task staff = taskService.createTaskQuery().taskAssignee("staff").singleResult();
        taskService.complete(staff.getId());
        log.info("用户任务：{}办理完成，办理人为：{}", staff.getName(), staff.getAssignee());
        //查询任务2
        Task boss = taskService.createTaskQuery().taskAssignee("boss").singleResult();
        taskService.complete(boss.getId());
        log.info("用户任务：{}办理完成，办理人为：{}", boss.getName(), boss.getAssignee());
        //查询当前系统中，还有多少个任务
        long taskNum = taskService.createTaskQuery().count();
        log.info("当前系统中，还有{}个任务", taskNum);
        //关闭流程引擎
        processEngine.close();
    }
}
