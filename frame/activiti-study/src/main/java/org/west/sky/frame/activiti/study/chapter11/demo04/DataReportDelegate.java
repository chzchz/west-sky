package org.west.sky.frame.activiti.study.chapter11.demo04;

import org.activiti.engine.delegate.BpmnError;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

/**
 * author: chz
 * date: 2025/2/21
 * description:
 */
public class DataReportDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("模拟数据上报发生异常");
        throw new BpmnError("theError");
    }
}
