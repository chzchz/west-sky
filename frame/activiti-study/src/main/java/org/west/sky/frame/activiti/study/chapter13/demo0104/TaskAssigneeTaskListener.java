package org.west.sky.frame.activiti.study.chapter13.demo0104;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

import java.util.Arrays;

/**
 * author: chz
 * date: 2025/3/13
 * description:
 */
public class TaskAssigneeTaskListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        switch (delegateTask.getName()){
            case "指定人提交":
                delegateTask.setAssignee("zhangsan");
                break;
            case "候选人审批":
                delegateTask.addCandidateUsers(Arrays.asList("manager1", "manager2"));
                break;
            case "候选组审批":
                delegateTask.addCandidateGroups(Arrays.asList("group1", "group2"));
                break;
            case "指定人审批":
                delegateTask.setAssignee(((TaskAssigneeBean)delegateTask.getVariables().get("taskAssigneeBean")).getApprover());
            default:
                break;
        }
    }
}
