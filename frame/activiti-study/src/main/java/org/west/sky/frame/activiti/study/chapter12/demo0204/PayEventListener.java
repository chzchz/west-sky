package org.west.sky.frame.activiti.study.chapter12.demo0204;

import org.activiti.engine.delegate.BpmnError;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.impl.bpmn.helper.ErrorPropagation;

/**
 * author: chz
 * date: 2025/3/12
 * description:
 */
public class PayEventListener implements TaskListener {

    private Integer account = 100;

    @Override
    public void notify(DelegateTask delegateTask) {
        Integer fee = (Integer) delegateTask.getVariable("fee");
        try {
            if (fee > account) {
                System.out.println("====余额不足");
                throw new BpmnError("500");
            } else {
                System.out.println("====扣款成功");
            }
        } catch (BpmnError e) {
            ErrorPropagation.propagateError(e, delegateTask.getExecution());
        }
    }
}
