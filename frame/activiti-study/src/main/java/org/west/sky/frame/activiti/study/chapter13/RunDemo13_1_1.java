package org.west.sky.frame.activiti.study.chapter13;

import lombok.SneakyThrows;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

/**
 * author: chz
 * date: 2025/3/12
 * description: 用户任务
 */
public class RunDemo13_1_1 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo13_1_1 runDemo13_1_1 = new RunDemo13_1_1();
        runDemo13_1_1.deploy();
    }

    @SneakyThrows
    private void deploy() {
        loadActivitiConfigAndInitEngine("activiti.cfg.xml");
        //部署流程
        ProcessDefinition processDefinition = deployByClassPathResource("processes/chapter13/Chapter13_1_1.bpmn20.xml");
        //开启流程实例
        ProcessInstance processInstance = runtimeService.startProcessInstanceById(processDefinition.getId());
        //完成任务
        Task task_1 = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("====当前流程：" + processInstance.getId() + "进行到：" + task_1.getName() + "：任务节点");
        taskService.complete(task_1.getId());

        Task task_2 = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("====当前流程：" + processInstance.getId() + "进行到：" + task_2.getName() + "：任务节点");
        System.out.println("====任务开始时间：" + sdf.format(task_2.getCreateTime()) + "任务过期时间：" + sdf.format(task_2.getDueDate()));
        taskService.complete(task_2.getId());

        closeEngine();
    }
}
