package org.west.sky.frame.activiti.study.chapter11.demo06;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

/**
 * author: chz
 * date: 2025/2/26
 * description:
 */
public class CompensationForCancelEndEventDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("============事件回滚，执行补偿操作============");
    }
}
