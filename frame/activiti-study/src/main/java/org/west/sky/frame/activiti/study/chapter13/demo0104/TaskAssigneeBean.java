package org.west.sky.frame.activiti.study.chapter13.demo0104;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * author: chz
 * date: 2025/3/13
 * description:
 */
public class TaskAssigneeBean implements Serializable {

    private String approver;

    /**
     * 发起人
     *
     * @return
     */
    public String getInitiator() {
        return "zhangsan";
    }

    /**
     * 候选人
     *
     * @return
     */
    public List<String> getCandidateUsers() {
        return Arrays.asList("manager1", "manager2");
    }

    /**
     * 候选组
     *
     * @return
     */
    public List<String> getCandidateGroups() {
        return Arrays.asList("group1", "group2");
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }
}
