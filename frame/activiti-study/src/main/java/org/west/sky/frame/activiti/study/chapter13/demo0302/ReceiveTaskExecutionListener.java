package org.west.sky.frame.activiti.study.chapter13.demo0302;

import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

/**
 * author: chz
 * date: 2025/3/14
 * description:
 */
public class ReceiveTaskExecutionListener implements ExecutionListener {
    @Override
    public void notify(DelegateExecution execution) {
        FlowElement flowElement = execution.getCurrentFlowElement();
        System.out.println("====监听到流程节点：" + flowElement.getId() + "，名称：" + flowElement.getName() + "，说明：" + flowElement.getDocumentation());
        System.out.println("====审核结果：" + execution.getVariable("approveResult"));
    }
}
