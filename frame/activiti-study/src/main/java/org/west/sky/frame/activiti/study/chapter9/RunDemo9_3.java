package org.west.sky.frame.activiti.study.chapter9;

import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;
import org.west.sky.frame.activiti.study.util.GroupUtil;
import org.west.sky.frame.activiti.study.util.UserUtil;

/**
 * author: chz
 * date: 2024/10/25
 * description: 创建用户组，管理用户和用户组关系
 */
public class RunDemo9_3 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo9_3 runDemo9_1 = new RunDemo9_3();
        runDemo9_1.runDemo();
    }

    public void runDemo() {
        loadActivitiConfigAndInitEngine("activiti.cfg.xml");

//        GroupUtil.addGroup(identityService, "group2", "group2名称", "group");

//        GroupUtil.executeList(identityService.createGroupQuery());

//        GroupUtil.executeList(identityService.createGroupQuery().groupType("group").orderByGroupId().desc());

//        GroupUtil.executePage(identityService.createGroupQuery().groupNameLike("group%"), 1, 1);

//        GroupUtil.executeCount(identityService.createGroupQuery().groupNameLike("group%"));

//        GroupUtil.executeSingleResult(identityService.createGroupQuery().groupName("group1名称"));

//        GroupUtil.updateGroup(identityService, "group2", "group3名称改", "group");

//        GroupUtil.deleteGroup(identityService, "group2");

//        GroupUtil.createMembership(identityService, "user1", "group1");

//        GroupUtil.deleteMembership(identityService, "user1", "group1");

//        GroupUtil.groupUserQuery(identityService, "group1");

        GroupUtil.userGroupQuery(identityService, "user1");
    }
}
