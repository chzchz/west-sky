package org.west.sky.frame.activiti.study.chapter10;

import org.activiti.engine.repository.Deployment;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

import java.util.List;

/**
 * author: chz
 * date: 2025/1/20
 * description: 查询部署记录
 */
public class RunDemo10_2 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo10_2 runDemo10_2 = new RunDemo10_2();
//        runDemo10_2.queryLastDeploymentByKey();
        runDemo10_2.queryDeploymentByKey();
    }

    /**
     * 根据key查询部署时间最近的一条记录
     */
    public void queryLastDeploymentByKey() {
        loadActivitiConfigAndInitEngine();
        Deployment key1 = repositoryService.createDeploymentQuery().deploymentKey("key1").latest().singleResult();
        System.out.println("部署ID：" + key1.getId());
    }

    public void queryDeploymentByKey() {
        loadActivitiConfigAndInitEngine();
        List<Deployment> deployments = repositoryService.createDeploymentQuery().deploymentKey("key1").orderByDeploymenTime().desc().list();
        for (Deployment deployment : deployments) {
            System.out.println("部署ID：" + deployment.getId());
        }
    }
}
