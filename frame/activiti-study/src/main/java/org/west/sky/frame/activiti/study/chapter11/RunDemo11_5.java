package org.west.sky.frame.activiti.study.chapter11;

import lombok.SneakyThrows;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * author: chz
 * date: 2025/2/20
 * description: 错误结束事件
 */
public class RunDemo11_5 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo11_5 runDemo11_1 = new RunDemo11_5();
        runDemo11_1.deploy();
    }

    @SneakyThrows
    public void deploy() {
        loadActivitiConfigAndInitEngine("activiti.job.xml");
        Deployment deploy = repositoryService.createDeployment().name("错误结束事件").addClasspathResource("processes/chapter11/Chapter11_5.bpmn20.xml")
                .deploy();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
//        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId("5").singleResult();
        ProcessInstance processInstance = runtimeService.startProcessInstanceById(processDefinition.getId());

        Task task1 = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("当前流程进行到：" + task1.getName() + "：任务节点");
        taskService.complete(task1.getId());

        Task task2 = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("当前流程进行到：" + task2.getName() + "：任务节点");
        Map<String, Object> variables = new HashMap<>();
        variables.put("payResult", true);
        taskService.complete(task2.getId(), variables);

        Task task3 = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("当前流程进行到：" + task3.getName() + "：任务节点");
        taskService.complete(task3.getId());

        Task task4 = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("当前流程进行到：" + task4.getName() + "：任务节点");
        taskService.complete(task4.getId());
        closeEngine();
    }
}
