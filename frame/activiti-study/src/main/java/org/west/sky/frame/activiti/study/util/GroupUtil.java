package org.west.sky.frame.activiti.study.util;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.identity.User;

import java.util.List;

/**
 * author: chz
 * date: 2024/11/19
 * description: 用户组创建工具类
 */
public class GroupUtil {

    /**
     * 添加用户组
     *
     * @param identityService
     * @param groupId
     * @param name
     * @param type
     */
    public static void addGroup(IdentityService identityService, String groupId, String name, String type) {
        Group group = identityService.newGroup(groupId);
        group.setName(name);
        group.setType(type);
        identityService.saveGroup(group);
    }

    /**
     * 查询列表
     *
     * @param groupQuery
     */
    public static void executeList(GroupQuery groupQuery) {
        List<Group> groups = groupQuery.list();
        for (Group group : groups) {
            System.out.println("用户组id:" + group.getId() + ",用户组名称:" + group.getName() + ",用户组类型:" + group.getType());
        }
    }

    /**
     * 查询分页
     *
     * @param groupQuery
     */
    public static void executePage(GroupQuery groupQuery, Integer firstResult, Integer maxResults) {
        List<Group> groups = groupQuery.listPage(firstResult, maxResults);
        for (Group group : groups) {
            System.out.println("用户组id:" + group.getId() + ",用户组名称:" + group.getName() + ",用户组类型:" + group.getType());
        }
    }

    /**
     * 查询统计
     *
     * @param groupQuery
     */
    public static void executeCount(GroupQuery groupQuery) {
        long count = groupQuery.count();
        System.out.println("用户组数量为:" + count);
    }

    /**
     * 查询单个结果
     *
     * @param groupQuery
     */
    public static void executeSingleResult(GroupQuery groupQuery) {
        Group group = groupQuery.singleResult();
        System.out.println("用户组id:" + group.getId() + ",用户组名称:" + group.getName() + ",用户组类型:" + group.getType());
    }

    /**
     * 更新用户组
     *
     * @param identityService
     * @param groupId
     * @param newName
     * @param newType
     */
    public static void updateGroup(IdentityService identityService, String groupId, String newName, String newType) {
        Group group = identityService.createGroupQuery().groupId(groupId).singleResult();
        group.setName(newName);
        group.setType(newType);
        identityService.saveGroup(group);
    }

    /**
     * 删除用户组
     *
     * @param identityService
     * @param groupId
     */
    public static void deleteGroup(IdentityService identityService, String groupId) {
        identityService.deleteGroup(groupId);
    }

    /**
     * 创建用户、用户组关联关系
     *
     * @param identityService
     * @param userId
     * @param groupId
     */
    public static void createMembership(IdentityService identityService, String userId, String groupId) {
        identityService.createMembership(userId, groupId);
    }

    /**
     * 移除 用户、用户组关联关系
     *
     * @param identityService
     * @param userId
     * @param groupId
     */
    public static void deleteMembership(IdentityService identityService, String userId, String groupId) {
        identityService.deleteMembership(userId, groupId);
    }

    /**
     * 组内用户查询
     *
     * @param identityService
     * @param groupId
     */
    public static void groupUserQuery(IdentityService identityService, String groupId) {
        List<User> list = identityService.createUserQuery().memberOfGroup(groupId).list();
        for (User user : list) {
            System.out.println(user.getFirstName() + user.getLastName());
        }
    }

    public static void userGroupQuery(IdentityService identityService, String userId){
        List<Group> groups = identityService.createGroupQuery().groupMember(userId).list();
        for (Group group : groups) {
            System.out.println(group.getName());
        }
    }
}
