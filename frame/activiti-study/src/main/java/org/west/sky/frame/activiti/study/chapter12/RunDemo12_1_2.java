package org.west.sky.frame.activiti.study.chapter12;

import lombok.SneakyThrows;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

/**
 * author: chz
 * date: 2025/2/20
 * description: 信号边界事件
 */
public class RunDemo12_1_2 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo12_1_2 runDemo11_1 = new RunDemo12_1_2();
        runDemo11_1.deploy();
    }

    @SneakyThrows
    public void deploy() {
        loadActivitiConfigAndInitEngine("activiti.job.xml");
        Deployment deploy = repositoryService.createDeployment().name("信号边界事件").addClasspathResource("processes/chapter12/Chapter12_1_2.bpmn20.xml")
                .deploy();
        ProcessDefinition definition = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();

        //开启两个流程实例
        ProcessInstance instance1 = runtimeService.startProcessInstanceById(definition.getId());
        ProcessInstance instance2 = runtimeService.startProcessInstanceById(definition.getId());

        Task task1_1 = taskService.createTaskQuery().processInstanceId(instance1.getId()).singleResult();
        System.out.println("====当前流程：" + instance1.getId() + "进行到：" + task1_1.getName() + "：任务节点");
        taskService.complete(task1_1.getId());

        Task task1_2 = taskService.createTaskQuery().processInstanceId(instance1.getId()).singleResult();
        System.out.println("====当前流程：" + instance1.getId() + "进行到：" + task1_2.getName() + "：任务节点");

        Task task2_1 = taskService.createTaskQuery().processInstanceId(instance2.getId()).singleResult();
        System.out.println("====当前流程：" + instance2.getId() + "进行到：" + task2_1.getName() + "：任务节点");
        taskService.complete(task2_1.getId());

        Task task2_2 = taskService.createTaskQuery().processInstanceId(instance2.getId()).singleResult();
        System.out.println("====当前流程：" + instance2.getId() + "进行到：" + task2_2.getName() + "：任务节点");

        runtimeService.signalEventReceived("修改信号");

        Task task1_3 = taskService.createTaskQuery().processInstanceId(instance1.getId()).singleResult();
        System.out.println("====当前流程：" + instance1.getId() + "进行到：" + task1_3.getName() + "：任务节点");

        Task task2_3 = taskService.createTaskQuery().processInstanceId(instance2.getId()).singleResult();
        System.out.println("====当前流程：" + instance2.getId() + "进行到：" + task2_3.getName() + "：任务节点");

        closeEngine();
    }
}
