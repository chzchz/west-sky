package org.west.sky.frame.activiti.study.chapter10;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

import java.util.List;

/**
 * author: chz
 * date: 2025/1/20
 * description: 查询流程定义
 */
public class RunDemo10_3 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo10_3 runDemo10_2 = new RunDemo10_3();
        runDemo10_2.queryLastDefinitionByKey();
//        runDemo10_2.queryDeploymentByKey();
    }

    /**
     * 根据key查询版本最新的一条流程定义
     */
    public void queryLastDefinitionByKey() {
        loadActivitiConfigAndInitEngine();
        ProcessDefinition key1 = repositoryService.createProcessDefinitionQuery().processDefinitionKey("Chapter8_4").latestVersion().singleResult();
        System.out.println("部署ID：" + key1.getId());
    }

    public void queryDeploymentByKey() {
        loadActivitiConfigAndInitEngine();
        List<Deployment> deployments = repositoryService.createDeploymentQuery().deploymentKey("key1").orderByDeploymenTime().desc().list();
        for (Deployment deployment : deployments) {
            System.out.println("部署ID：" + deployment.getId());
        }
    }
}
