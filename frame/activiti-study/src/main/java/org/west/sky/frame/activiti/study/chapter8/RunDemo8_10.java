package org.west.sky.frame.activiti.study.chapter8;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.management.TableMetaData;
import org.activiti.engine.management.TablePage;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

import java.util.List;
import java.util.Map;

/**
 * author: chz
 * date: 2024/9/6
 * description: 数据库管理
 */
public class RunDemo8_10 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo8_10 runDemo8_3 = new RunDemo8_10();
        runDemo8_3.runDemo();
    }

    private void runDemo() {
        loadActivitiConfigAndInitEngine("activiti.cfg.xml");
        //获取表的记录条数
        Map<String, Long> tableCount = managementService.getTableCount();
        tableCount.forEach((k, v) -> System.out.println("表名：" + k + "，行数：" + v));
        //获取数据表名称及元数据
        String tableName = managementService.getTableName(HistoricProcessInstance.class);
        TableMetaData tableMetaData = managementService.getTableMetaData(tableName);
        List<String> columnNames = tableMetaData.getColumnNames();
        columnNames.forEach(e -> System.out.println("--字段：" + e));

        //获取表的分页查询对象
        TablePage tablePage = managementService.createTablePageQuery().tableName(tableName).orderAsc("START_TIME_").listPage(0, 10);
        System.out.println(tableName + "的记录条数为：" + tablePage.getTotal());
        processEngine.close();
    }

}
