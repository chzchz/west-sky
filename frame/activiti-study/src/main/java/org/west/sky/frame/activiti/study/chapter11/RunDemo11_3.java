package org.west.sky.frame.activiti.study.chapter11;

import lombok.SneakyThrows;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

/**
 * author: chz
 * date: 2025/2/20
 * description: 消息开始事件
 */
public class RunDemo11_3 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo11_3 runDemo11_1 = new RunDemo11_3();
        runDemo11_1.deploy();
    }

    @SneakyThrows
    public void deploy() {
        loadActivitiConfigAndInitEngine("activiti.job.xml");
        Deployment deploy = repositoryService.createDeployment().name("消息开始事件").addClasspathResource("processes/chapter11/Chapter11_3.bpmn20.xml")
                .deploy();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
        Task task = taskService.createTaskQuery().processDefinitionId(processDefinition.getId()).singleResult();
        if (null == task) {
            System.out.println("流程实例尚未开启");
        }
        runtimeService.startProcessInstanceByMessage("the Message");
        Task task1 = taskService.createTaskQuery().processDefinitionId(processDefinition.getId()).singleResult();
        System.out.println("第一个任务id:" + task1.getId() + "，任务名称:" + task1.getName());
        taskService.complete(task1.getId());
        closeEngine();
    }
}
