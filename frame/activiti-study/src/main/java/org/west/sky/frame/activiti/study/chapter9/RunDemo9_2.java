package org.west.sky.frame.activiti.study.chapter9;

import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;
import org.west.sky.frame.activiti.study.util.UserUtil;

/**
 * author: chz
 * date: 2024/10/25
 * description: 用户增删改查
 */
public class RunDemo9_2 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo9_2 runDemo9_1 = new RunDemo9_2();
        runDemo9_1.runDemo();
    }

    public void runDemo() {
        loadActivitiConfigAndInitEngine("activiti.cfg.xml");

//        UserUtil.executeList(identityService.createUserQuery().userFirstNameLike("user%"));

//        UserUtil.executeListPage(identityService.createUserQuery(), 0, 2);

//        UserUtil.executeCount(identityService.createUserQuery().userFirstNameLike("user%"));

//        UserUtil.executeSingleResult(identityService.createUserQuery().userFirstNameLike("user%"));
//        UserUtil.updateUser(identityService,"ls","犁", "思", "ls@173.com", "1234567");
//        UserUtil.deleteUser(identityService, "ls");
//        UserUtil.setUserPic(identityService, "user1",ClassLoader.getSystemResource("pictures/kt1.jpg").getPath());
//        UserUtil.setUserInfo(identityService, "user1", "sex", "未知");
//        UserUtil.deleteUserInfo(identityService, "user1", "sex");
//        UserUtil.getUserInfo(identityService, "user1", "age");
        UserUtil.getUserInfoKeys(identityService, "user1");
    }
}
