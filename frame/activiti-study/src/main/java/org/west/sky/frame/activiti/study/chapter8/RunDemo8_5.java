package org.west.sky.frame.activiti.study.chapter8;

import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

/**
 * author: chz
 * date: 2024/9/6
 * description: 创建流程实例
 */
public class RunDemo8_5 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo8_5 runDemo8_3 = new RunDemo8_5();
        runDemo8_3.runDemo();
    }

    private void runDemo() {
        loadActivitiConfigAndInitEngine("activiti.cfg.xml");
        ProcessDefinition processDefinition = deployByClassPathResource("processes/chapter8/Chapter8_1.bpmn20.xml");
        System.out.println("流程定义ID:" + processDefinition.getId() + ",流程定义key:" + processDefinition.getKey()
                + ",是否挂起：" + processDefinition.isSuspended());
        //根据流程定义id发起流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceById(processDefinition.getId());
        queryProcessInstance(processInstance.getId());
        //根据流程定义key发起流程
        ProcessInstance processInstance2 = runtimeService.createProcessInstanceBuilder().processDefinitionKey(processDefinition.getKey()).name("Chapter8_1Instance")
                .start();
        queryProcessInstance(processInstance2.getId());
        processEngine.close();
    }


    private void queryProcessInstance(String id) {
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(id).singleResult();
        System.out.println("发起流程实例成功，流程实例ID：" + processInstance.getId()
                + ",流程定义ID：" + processInstance.getProcessDefinitionId()
                + "流程实例名称：" + processInstance.getName());
    }
}
