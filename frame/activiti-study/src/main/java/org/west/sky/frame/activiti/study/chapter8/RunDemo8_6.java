package org.west.sky.frame.activiti.study.chapter8;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

/**
 * author: chz
 * date: 2024/9/6
 * description: 唤醒一个等待流程
 */
public class RunDemo8_6 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo8_6 runDemo8_3 = new RunDemo8_6();
        runDemo8_3.runDemo();
    }

    private void runDemo() {
        loadActivitiConfigAndInitEngine("activiti.cfg.xml");
        ProcessDefinition processDefinition = deployByClassPathResource("processes/chapter8/Chapter8_2.bpmn20.xml");
        System.out.println("流程定义ID:" + processDefinition.getId() + ",流程定义key:" + processDefinition.getKey());
        //根据流程定义id发起流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceById(processDefinition.getId());
        System.out.println("发起流程实例成功，流程实例id:" + processInstance.getId());

        //查询第一个任务
        Task firstTask = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("第一个任务id:" + firstTask.getId() + ",任务名称：" + firstTask.getName());
        taskService.setAssignee(firstTask.getId(), "张三");
        taskService.complete(firstTask.getId());

        //查询当前执行
        Execution execution = runtimeService.createExecutionQuery().processInstanceId(processInstance.getId()).onlyChildExecutions().singleResult();
        System.out.println("当前执行id：" + execution.getId() + ",当前执行名称：" + execution.getName());
        runtimeService.trigger(execution.getId());
        System.out.println("触发机器节点，继续流转");

        HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("流程开始时间：" + sdf.format(historicProcessInstance.getStartTime()) + "，流程结束时间：" + sdf.format(historicProcessInstance.getEndTime()));
        processEngine.close();
    }


}
