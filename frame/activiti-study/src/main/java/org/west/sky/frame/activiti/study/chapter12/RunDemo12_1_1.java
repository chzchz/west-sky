package org.west.sky.frame.activiti.study.chapter12;

import lombok.SneakyThrows;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

/**
 * author: chz
 * date: 2025/2/20
 * description: 定时器边界事件
 */
public class RunDemo12_1_1 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo12_1_1 runDemo11_1 = new RunDemo12_1_1();
        runDemo11_1.deploy();
    }

    @SneakyThrows
    public void deploy() {
        loadActivitiConfigAndInitEngine("activiti.job.xml");
//        Deployment deploy = repositoryService.createDeployment().name("定时器边界事件").addClasspathResource("processes/chapter/Chapter12_1_1.bpmn20.xml")
//                .deploy();
        ProcessDefinition definition = repositoryService.createProcessDefinitionQuery().deploymentId("1").singleResult();

        ProcessInstance instance = runtimeService.startProcessInstanceById(definition.getId());

        Task task1 = taskService.createTaskQuery().processInstanceId(instance.getId()).singleResult();
        System.out.println("====当前流程进行到：" + task1.getName() + "：任务节点");
        taskService.complete(task1.getId());

        Task task2 = null;
//        taskService.complete(task2.getId());

        int i = 0;
        while (i < 13) {
            task2 = taskService.createTaskQuery().processInstanceId(instance.getId()).singleResult();
            System.out.println("====当前流程进行到：" + task2.getName() + "：任务节点");
            Thread.sleep(10 * 1000);
            i++;
        }

        Task task3 = taskService.createTaskQuery().processInstanceId(instance.getId()).singleResult();
        System.out.println("====当前流程进行到：" + task3.getName() + "：任务节点");
        taskService.complete(task3.getId());

        Task task4 = taskService.createTaskQuery().processInstanceId(instance.getId()).singleResult();
        System.out.println("====当前流程进行到：" + task4.getName() + "：任务节点");
        taskService.complete(task4.getId());

        closeEngine();
    }
}
