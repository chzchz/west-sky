package org.west.sky.frame.activiti.study.chapter9;

import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;
import org.west.sky.frame.activiti.study.util.UserUtil;

/**
 * author: chz
 * date: 2024/10/25
 * description: 创建用户
 */
public class RunDemo9_1 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo9_1 runDemo9_1 = new RunDemo9_1();
        runDemo9_1.runDemo();
    }

    public void runDemo() {
        loadActivitiConfigAndInitEngine("activiti.cfg.xml");
        UserUtil.addUser(identityService, "user1", "user1", "user1", "user1@163.com", "123456");
    }
}
