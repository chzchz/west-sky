package org.west.sky.frame.activiti.study.chapter8;

import org.activiti.engine.repository.ProcessDefinition;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

/**
 * author: chz
 * date: 2024/9/6
 * description: 激活流程定义
 */
public class RunDemo8_4 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo8_4 runDemo8_3 = new RunDemo8_4();
        runDemo8_3.runDemo();
    }

    private void runDemo() {
        loadActivitiConfigAndInitEngine("activiti.cfg.xml");
        ProcessDefinition processDefinition = deployByClassPathResource("processes/chapter8/Chapter8_1.bpmn20.xml");
        queryProcessDefinition(processDefinition.getId());
        //挂起流程
        repositoryService.suspendProcessDefinitionById(processDefinition.getId());
        System.out.println("挂起流程成功,流程定义ID:" + processDefinition.getId());
        //再次查询流程定义和部署流程
        queryProcessDefinition(processDefinition.getId());
        //激活流程
        repositoryService.activateProcessDefinitionById(processDefinition.getId());
        //再次查询流程定义和部署流程
        queryProcessDefinition(processDefinition.getId());
        processEngine.close();
    }


    private void queryProcessDefinition(String id) {
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionId(id).singleResult();
        System.out.println("流程定义ID:" + processDefinition.getId() + ",流程定义key:" + processDefinition.getKey()
                + ",是否挂起：" + processDefinition.isSuspended());
    }
}
