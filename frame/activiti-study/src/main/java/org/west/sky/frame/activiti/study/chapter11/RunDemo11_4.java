package org.west.sky.frame.activiti.study.chapter11;

import lombok.SneakyThrows;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

/**
 * author: chz
 * date: 2025/2/20
 * description: 错误开始事件
 */
public class RunDemo11_4 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo11_4 runDemo11_1 = new RunDemo11_4();
        runDemo11_1.deploy();
    }

    @SneakyThrows
    public void deploy() {
        loadActivitiConfigAndInitEngine("activiti.job.xml");
        Deployment deploy = repositoryService.createDeployment().name("错误开始事件").addClasspathResource("processes/chapter11/Chapter11_4.bpmn20.xml")
                .deploy();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
        runtimeService.startProcessInstanceById(processDefinition.getId());
        closeEngine();
    }
}
