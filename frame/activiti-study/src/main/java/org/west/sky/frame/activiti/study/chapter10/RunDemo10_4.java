package org.west.sky.frame.activiti.study.chapter10;

import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.persistence.entity.DeploymentEntity;
import org.activiti.engine.impl.persistence.entity.DeploymentEntityImpl;
import org.activiti.engine.impl.persistence.entity.ResourceEntity;
import org.activiti.engine.impl.persistence.entity.ResourceEntityManager;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

import java.util.List;
import java.util.Map;

/**
 * author: chz
 * date: 2025/1/20
 * description: 查询流程资源
 */
public class RunDemo10_4 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo10_4 runDemo10_2 = new RunDemo10_4();
        runDemo10_2.getResourcesWithDeployment();
    }

    public void getResourcesWithDeployment() {
        loadActivitiConfigAndInitEngine();
        ProcessEngineConfigurationImpl processEngineConfiguration = (ProcessEngineConfigurationImpl) configuration.getProcessEngineConfiguration();
        ResourceEntityManager entityManager = processEngineConfiguration.getResourceEntityManager();
        List<Deployment> deployments = repositoryService.createDeploymentQuery().deploymentKey("key1").orderByDeploymenTime().desc().list();
        for (Deployment deployment : deployments) {
            System.out.println("部署ID：" + deployment.getId());
            List<ResourceEntity> resourceEntityList = entityManager.findResourcesByDeploymentId(deployment.getId());
            for (ResourceEntity resourceEntity : resourceEntityList) {
                System.out.println("资源名称：" + resourceEntity.getName());
            }
        }
    }
}
