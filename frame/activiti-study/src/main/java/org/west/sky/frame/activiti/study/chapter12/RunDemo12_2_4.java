package org.west.sky.frame.activiti.study.chapter12;

import lombok.SneakyThrows;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author: chz
 * date: 2025/2/20
 * description: 补偿中间抛出事件
 */
public class RunDemo12_2_4 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo12_2_4 runDemo11_1 = new RunDemo12_2_4();
        runDemo11_1.deploy();
    }

    @SneakyThrows
    public void deploy() {
        loadActivitiConfigAndInitEngine("activiti.job.xml");
        Deployment deploy = repositoryService.createDeployment().name("补偿中间抛出事件").addClasspathResource("processes/chapter12/Chapter12_2_4.bpmn20.xml")
                .deploy();
        ProcessDefinition definition = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();

        ProcessInstance instance1 = runtimeService.startProcessInstanceById(definition.getId());

        Task task1_1 = taskService.createTaskQuery().processInstanceId(instance1.getId()).taskName("预报名").singleResult();
        System.out.println("====当前流程：" + instance1.getId() + "进行到：" + task1_1.getName() + "：任务节点");
        Map<String, Object> variables1 = new HashMap<>();
        variables1.put("name", "张三");
        variables1.put("grade", "一年级");
        taskService.complete(task1_1.getId(), variables1);

        Task task1_2 = taskService.createTaskQuery().processInstanceId(instance1.getId()).taskName("正式报名").singleResult();
        System.out.println("====当前流程：" + instance1.getId() + "进行到：" + task1_2.getName() + "：任务节点");
        taskService.complete(task1_2.getId());

        Task task1_3 = taskService.createTaskQuery().processInstanceId(instance1.getId()).taskName("报名审核").singleResult();
        System.out.println("====当前流程：" + instance1.getId() + "进行到：" + task1_3.getName() + "：任务节点");
        taskService.complete(task1_3.getId());

        Task task1_4 = taskService.createTaskQuery().processInstanceId(instance1.getId()).taskName("刷卡付钱").singleResult();
        System.out.println("====当前流程：" + instance1.getId() + "进行到：" + task1_4.getName() + "：任务节点");
        Map<String, Object> variables2 = new HashMap<>();
        variables2.put("fee", 1000);
        taskService.complete(task1_4.getId(), variables2);

        closeEngine();
    }
}
