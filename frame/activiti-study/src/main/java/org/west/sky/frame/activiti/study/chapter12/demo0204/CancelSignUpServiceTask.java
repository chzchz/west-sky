package org.west.sky.frame.activiti.study.chapter12.demo0204;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

/**
 * author: chz
 * date: 2025/3/12
 * description:
 */
public class CancelSignUpServiceTask implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("====取消正式报名成功");
    }
}
