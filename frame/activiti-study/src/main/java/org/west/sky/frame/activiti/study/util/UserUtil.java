package org.west.sky.frame.activiti.study.util;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Picture;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * author: chz
 * date: 2024/10/25
 * description: 用户创建工具类
 */
public class UserUtil {

    /**
     * 创建用户
     *
     * @param identityService
     * @param userId
     * @param lastName
     * @param firstName
     * @param email
     * @param password
     */
    public static void addUser(IdentityService identityService, String userId, String lastName, String firstName, String email, String password) {
        User user = identityService.newUser(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(password);
        identityService.saveUser(user);
    }

    public static void executeList(UserQuery query) {
        List<User> list = query.list();
        for (User user : list) {
            System.out.printf("用户id:%s,用户姓名： %s %s，用户邮箱 %s%n", user.getId(), user.getFirstName(), user.getLastName(), user.getEmail());
        }
    }

    public static void executeListPage(UserQuery query, Integer firstResult, Integer maxResults) {
        List<User> list = query.listPage(firstResult, maxResults);
        for (User user : list) {
            System.out.printf("用户id:%s,用户姓名： %s %s，用户邮箱 %s%n", user.getId(), user.getFirstName(), user.getLastName(), user.getEmail());
        }
    }

    public static void executeCount(UserQuery query) {
        long count = query.count();
        System.out.printf("用户数为:%s%n", count);
    }

    public static void executeSingleResult(UserQuery query) {
        User user = query.singleResult();
        System.out.printf("用户id:%s,用户姓名： %s %s，用户邮箱 %s%n", user.getId(), user.getFirstName(), user.getLastName(), user.getEmail());
    }

    /**
     * 更新用户
     *
     * @param identityService
     * @param userId
     * @param newLastName
     * @param newFirstName
     * @param newEmail
     * @param newPassword
     */
    public static void updateUser(IdentityService identityService, String userId, String newLastName, String newFirstName, String newEmail, String newPassword) {
        User user = identityService.createUserQuery().userId(userId).singleResult();
        user.setFirstName(newFirstName);
        user.setLastName(newLastName);
        user.setEmail(newEmail);
        user.setPassword(newPassword);
        identityService.saveUser(user);
    }

    /**
     * 删除用户
     *
     * @param identityService
     * @param userId
     */
    public static void deleteUser(IdentityService identityService, String userId) {
        identityService.deleteUser(userId);
    }

    /**
     * 设置用户图片
     *
     * @param identityService
     * @param userId
     * @param picPath
     */
    public static void setUserPic(IdentityService identityService, String userId, String picPath) {
        try {
            File file = new File(picPath);
            BufferedImage image = ImageIO.read(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(image, "png", bos);
            Picture picture = new Picture(bos.toByteArray(), "picture of userId:" + userId);
            identityService.setUserPicture(userId, picture);
        } catch (IOException e) {
            System.out.println("文件:" + picPath + "，不存在");
        }
    }

    /**
     * 设置用户附属信息
     *
     * @param identityService
     * @param userId
     * @param key
     * @param value
     */
    public static void setUserInfo(IdentityService identityService, String userId, String key, String value) {
        identityService.setUserInfo(userId, key, value);
    }

    /**
     * 删除用户附属信息
     *
     * @param identityService
     * @param userId
     * @param key
     */
    public static void deleteUserInfo(IdentityService identityService, String userId, String key) {
        identityService.deleteUserInfo(userId, key);
    }

    /**
     * 获取用户附属信息
     *
     * @param identityService
     * @param userId
     * @param key
     */
    public static void getUserInfo(IdentityService identityService, String userId, String key) {
        String value = identityService.getUserInfo(userId, key);
        System.out.println("key:" + key + ",value:" + value);
    }

    public static void getUserInfoKeys(IdentityService identityService, String userId){
        List<String> keys = identityService.getUserInfoKeys(userId);
        for (String key : keys) {
            System.out.println(key);
        }
    }
}
