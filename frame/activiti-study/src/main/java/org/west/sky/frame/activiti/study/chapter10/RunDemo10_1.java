package org.west.sky.frame.activiti.study.chapter10;

import org.activiti.bpmn.model.*;
import org.activiti.bpmn.model.Process;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

import java.io.*;
import java.util.zip.ZipInputStream;

/**
 * author: chz
 * date: 2024/11/21
 * description: 流程部署
 */
public class RunDemo10_1 extends ActivitiEngineUtil {

    public static void main(String[] args) throws IOException {
        RunDemo10_1 runDemo10_1 = new RunDemo10_1();
//        runDemo10_1.deployByInputStream();
//        runDemo10_1.deployByClasspathResource();
//        runDemo10_1.deployByString();
//        runDemo10_1.deployByBytes();
//        runDemo10_1.deployByZip();
        runDemo10_1.deployByBpmnModel();
    }

    /**
     * bpmn模型部署
     */
    public void deployByBpmnModel(){
        loadActivitiConfigAndInitEngine();
        DeploymentBuilder deployment = repositoryService.createDeployment();
        BpmnModel bpmnModel = new BpmnModel();
        //创建流程
        Process process = new Process();
        process.setId("deployByBpmnModel");
        process.setName("请假申请流程");
        bpmnModel.addProcess(process);
        //创建开始节点
        StartEvent startEvent = new StartEvent();
        startEvent.setId("startEvent");
        startEvent.setName("开始");
        process.addFlowElement(startEvent);
        //创建申请任务节点
        UserTask userTask1= new UserTask();
        userTask1.setId("userTask1");
        userTask1.setName("申请");
        process.addFlowElement(userTask1);
        //创建审批任务节点
        UserTask userTask2 = new UserTask();
        userTask2.setId("userTask2");
        userTask2.setName("审批");
        process.addFlowElement(userTask2);
        //创建结束节点
        EndEvent endEvent = new EndEvent();
        endEvent.setId("endEvent");
        endEvent.setName("结束");
        process.addFlowElement(endEvent);
        //创建节点关系
        process.addFlowElement(new SequenceFlow("startEvent", "userTask1"));
        process.addFlowElement(new SequenceFlow("userTask1", "userTask2"));
        process.addFlowElement(new SequenceFlow("userTask2", "endEvent"));
        deployment.addBpmnModel("Chapter8_1.bpmn20.xml", bpmnModel);
        deployment.deploy();
    }

    /**
     * Zip压缩包部署
     */
    public void deployByZip() throws IOException {
        loadActivitiConfigAndInitEngine();
        try (FileInputStream fileInputStream = new FileInputStream("E:\\work_space\\west-sky\\frame\\activiti-study\\src\\main\\resources\\processes\\Chapter8_1.zip")) {
            ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);
            DeploymentBuilder deployment = repositoryService.createDeployment();
            deployment.addZipInputStream(zipInputStream);
            deployment.deploy();
        }
    }

    /**
     * 字节数组部署
     *
     * @throws IOException
     */
    public void deployByBytes() throws IOException {
        loadActivitiConfigAndInitEngine();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try (FileInputStream fileInputStream = new FileInputStream("E:\\work_space\\west-sky\\frame\\activiti-study\\src\\main\\resources\\processes\\Chapter8_1.bpmn20.xml")) {
            byte[] bytes = new byte[1024];
            int n;
            while ((n = fileInputStream.read(bytes)) != -1) {
                bos.write(bytes, 0, n);
            }
            bos.close();
        }
        DeploymentBuilder deployment = repositoryService.createDeployment();
        deployment.addBytes("Chapter8_1.bpmn20.xml", bos.toByteArray());
        deployment.deploy();
    }

    /**
     * 字符串资源部署
     *
     * @throws IOException
     */
    public void deployByString() throws IOException {
        loadActivitiConfigAndInitEngine();
        StringBuilder sb = new StringBuilder();
        try (FileReader fileReader = new FileReader("E:\\work_space\\west-sky\\frame\\activiti-study\\src\\main\\resources\\processes\\Chapter8_1.bpmn20.xml")) {
            BufferedReader br = new BufferedReader(fileReader);
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        }
        DeploymentBuilder deployment = repositoryService.createDeployment();
        deployment.addString("Chapter8_1.bpmn20.xml", sb.toString());
        deployment.deploy();
    }

    /**
     * Classpath下文件资源部署
     */
    public void deployByClasspathResource() {
        loadActivitiConfigAndInitEngine();
        DeploymentBuilder deployment = repositoryService.createDeployment();
        //自动将资源名作为文件名存储
        deployment.addClasspathResource("processes/chapter8/Chapter8_1.bpmn20.xml");
        deployment.deploy();
    }

    /**
     * 输入流资源部署
     *
     * @throws IOException
     */
    public void deployByInputStream() throws IOException {
        loadActivitiConfigAndInitEngine();
        try (FileInputStream fileInputStream = new FileInputStream("E:\\work_space\\west-sky\\frame\\activiti-study\\src\\main\\resources\\processes\\Chapter8_1.bpmn20.xml")) {
            DeploymentBuilder deployment = repositoryService.createDeployment();
            deployment.addInputStream("Chapter8_1.bpmn20.xml", fileInputStream);
            Deployment deploy = deployment.deploy();
            System.out.println("部署ID：" + deploy.getId());
            ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
            System.out.println("流程部署ID:" + deploy.getId() + ",流程名称:" + processDefinition.getName() + ",流程定义ID:" + processDefinition.getId()
                    + ",流程版本：" + processDefinition.getVersion());
        }
    }
}
