package org.west.sky.frame.activiti.study.chapter11;

import lombok.SneakyThrows;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElementsContainer;
import org.activiti.bpmn.model.SubProcess;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.west.sky.frame.activiti.study.util.ActivitiEngineUtil;

/**
 * author: chz
 * date: 2025/2/20
 * description: 取消结束事件
 */
public class RunDemo11_6 extends ActivitiEngineUtil {

    public static void main(String[] args) {
        RunDemo11_6 runDemo11_1 = new RunDemo11_6();
        runDemo11_1.deploy();
    }

    @SneakyThrows
    public void deploy() {
        loadActivitiConfigAndInitEngine("activiti.job.xml");
        Deployment deploy = repositoryService.createDeployment().name("取消结束事件").addClasspathResource("processes/chapter11/Chapter11_6.bpmn20.xml")
                .deploy();
        ProcessDefinition definition = repositoryService.createProcessDefinitionQuery().deploymentId(deploy.getId()).singleResult();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(definition.getId());
        System.out.println("=========流程定义name：" + definition.getName());
        ProcessInstance processInstance = runtimeService.startProcessInstanceById(definition.getId());

        //查询第一个任务
        Task firstTask = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("=========第一个任务id:" + firstTask.getId() + ",任务名称：" + firstTask.getName());
        FlowElementsContainer parentContainer = bpmnModel.getFlowElement(firstTask.getTaskDefinitionKey()).getParentContainer();
        System.out.println("=========第一个任务:" + firstTask.getName() + ",处于：" + (parentContainer instanceof SubProcess ? "子流程" : "主流程"));
        taskService.complete(firstTask.getId());

        Task task = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
        System.out.println("=========第二个任务id:" + task.getId() + ",任务名称：" + task.getName());
        FlowElementsContainer parentContainer2 = bpmnModel.getFlowElement(task.getTaskDefinitionKey()).getParentContainer();
        System.out.println("=========第二个任务:" + firstTask.getName() + ",处于：" + (parentContainer2 instanceof SubProcess ? "子流程" : "主流程"));
        taskService.complete(task.getId());
        closeEngine();
    }
}
