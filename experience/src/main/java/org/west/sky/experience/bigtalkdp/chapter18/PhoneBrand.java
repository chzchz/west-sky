package org.west.sky.experience.bigtalkdp.chapter18;

/**
 * 手机品牌抽象
 */
public abstract class PhoneBrand {

    protected Soft soft;

    public void setSoft(Soft soft) {
        this.soft = soft;
    }

    public abstract void play();
}
