package org.west.sky.experience.bigtalkdp.chapter09;

/**
 * @author chenghz
 * @date 2022/12/6 19:07
 * @description:
 */
public class Test {

    public static void main(String[] args) {

        ConcreteBuilderOne one = new ConcreteBuilderOne();
        ConcreteBuilderTwo two = new ConcreteBuilderTwo();

        Director director = new Director();
        director.Construct(one);
        Product productOne = one.getProduct();
        productOne.show();

        director.Construct(two);
        Product productTwo = two.getProduct();
        productTwo.show();

    }
}
