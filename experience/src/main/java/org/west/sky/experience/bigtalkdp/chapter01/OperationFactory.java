package org.west.sky.experience.bigtalkdp.chapter01;

/**
 * @author chenghz
 * @date 2022/9/12 9:57
 * @description:
 */
public class OperationFactory {

    public static Operation createOperation(String operate) {
        Operation operation;

        switch (operate) {
            case "+":
                operation = new OperationAdd();
                break;
            case "-":
                operation = new OperationSub();
                break;
            case "*":
                operation = new OperationMul();
                break;
            case "/":
                operation = new OperationDiv();
                break;
            default:
                throw new RuntimeException("功能暂未开放");
        }
        return operation;
    }
}
