package org.west.sky.experience.thinkinginjava.chapter05;

/**
 * @Author: chz
 * @Description:
 * @Date: 2023/3/9
 */
public class Dog {

    Cat cat1 = new Cat("jerry");
    static Cat cat2 = new Cat("tom");

    public Dog() {
        System.out.println("i am a dog");
    }

    public void bake(){
        System.out.println("wang wang");
    }
}
