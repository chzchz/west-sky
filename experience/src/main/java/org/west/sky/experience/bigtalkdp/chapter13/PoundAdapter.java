package org.west.sky.experience.bigtalkdp.chapter13;

/**
 * @author chenghz
 * @date 2022/12/28 15:22
 * @description: 英镑适配器
 */
public class PoundAdapter implements IMoney{

    private final Pound pound = new Pound();

    @Override
    public void buy() {
        System.out.println("100人民币=10英镑");
        pound.usePoundBuy();
    }
}
