package org.west.sky.experience.bigtalkdp.chapter19;

/**
 * 命令抽象接口
 */
public abstract class Command {

    protected GameCharacter gameCharacter;

    public void setGameCharacter(GameCharacter gameCharacter) {
        this.gameCharacter = gameCharacter;
    }

    public abstract void execute();
}
