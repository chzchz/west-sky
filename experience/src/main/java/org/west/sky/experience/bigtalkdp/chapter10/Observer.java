package org.west.sky.experience.bigtalkdp.chapter10;

/**
 * @author chenghz
 * @date 2022/12/26 16:18
 * @description: 抽象观察者
 */
public interface Observer {

    /**
     * 更新方法
     */
    void update();
}
