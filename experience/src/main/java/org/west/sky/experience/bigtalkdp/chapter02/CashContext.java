package org.west.sky.experience.bigtalkdp.chapter02;

/**
 * @author chenghz
 * @date 2022/10/31 15:07
 * @description: 策略上下文
 */
public class CashContext {

    private CashSuper cashSuper;

    public CashContext(String type) {
        switch (type) {
            case "正常收费":
                cashSuper = new CashNormal();
                break;
            case "满100减20":
                cashSuper = new CashReturn(100d, 20d);
                break;
            case "打8折":
                cashSuper = new CashRebate(0.8);
                break;
            default:
        }
    }

    public double getResult(double money){
        return cashSuper.acceptCash(money);
    }
}
