package org.west.sky.experience.bigtalkdp.chapter18;

/**
 * @author: chz
 * @date: 2023/7/12
 * @description:
 */
public class MusicSoft implements Soft {
    @Override
    public void play() {
        System.out.print("播放音乐");
    }
}
