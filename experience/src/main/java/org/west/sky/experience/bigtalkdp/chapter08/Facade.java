package org.west.sky.experience.bigtalkdp.chapter08;

/**
 * @author chenghz
 * @date 2022/12/5 17:24
 * @description: 外观模式-高层接口
 */
public class Facade {

    private SubSystemOne one;
    private SubSystemTwo two;
    private SubSystemThree three;
    private SubSystemFour four;

    public Facade() {
        this.one = new SubSystemOne();
        this.two = new SubSystemTwo();
        this.three = new SubSystemThree();
        this.four = new SubSystemFour();
    }

    public void methodA() {
        System.out.println("高层接口A.....");
        one.methodOne();
        two.methodTwo();
    }

    public void methodB() {
        System.out.println("高层接口B.....");
        three.methodThree();
        four.methodFour();
    }
}
