package org.west.sky.experience.bigtalkdp.chapter08;

/**
 * @author chenghz
 * @date 2022/12/5 19:47
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        Facade facade = new Facade();

        facade.methodA();
        facade.methodB();
    }
}
