package org.west.sky.experience.thinkinginjava.chapter05;

/**
 * @Author: chz
 * @Description: 静态成员初始化
 * @Date: 2023/3/9
 */
public class Test {

    static Dog dog1;

    public static void main(String[] args) {
        dog2.bake();
    }

    static Dog dog2 = new Dog();
}
