package org.west.sky.experience.bigtalkdp.chapter03;

/**
 * @author chenghz
 * @date 2022/11/21 20:11
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        Person p = new Person();
        p.setName("张三");
        Shirt shirt = new Shirt();
        shirt.decorate(p);
        Glass glass = new Glass();
        glass.decorate(shirt);
        Trousers trousers = new Trousers();
        trousers.decorate(glass);
        Shoes shoes = new Shoes();
        shoes.decorate(trousers);
        shoes.show();
    }
}
