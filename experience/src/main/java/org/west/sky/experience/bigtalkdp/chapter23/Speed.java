package org.west.sky.experience.bigtalkdp.chapter23;

/**
 * @author: chz
 * @date: 2023/9/13
 * @description: 演奏速度
 */
public class Speed extends Expression {
    @Override
    public void execute(String key, double value) {
        String speed = "";
        if (value < 500) {
            speed = "快速";
        } else if (value > 1000) {
            speed = "慢速";
        } else {
            speed = "中速";
        }
        System.out.print(speed + " ");
    }
}
