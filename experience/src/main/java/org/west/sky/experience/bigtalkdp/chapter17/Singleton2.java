package org.west.sky.experience.bigtalkdp.chapter17;

/**
 * @author: chz
 * @date: 2023/7/3
 * @description: 单例模式--饿汉模式
 */
public class Singleton2 {

    private static final Singleton2 singleton = new Singleton2();

    private Singleton2() {

    }

    public static Singleton2 instance() {
        return singleton;
    }
}
