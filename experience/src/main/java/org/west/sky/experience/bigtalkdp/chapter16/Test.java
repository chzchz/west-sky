package org.west.sky.experience.bigtalkdp.chapter16;

/**
 * @author: chz
 * @date: 2023/6/28
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        Mist<String> mist = new BrrayList<>();
        mist.add("111");
        mist.add("222");
        mist.add("333");
        mist.add("444");
        mist.add("555");
        mist.add("666");
        mist.add("777");

        System.out.println("-----正序遍历-----");
        Iterator<String> iterator = mist.createIterator();
        System.out.println(iterator.first());
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println("-----倒序遍历-----");
        Iterator<String> iteratorDesc = mist.createIteratorDesc();
        System.out.println(iteratorDesc.first());
        while (iteratorDesc.hasNext()) {
            System.out.println(iteratorDesc.next());
        }
    }
}
