package org.west.sky.experience.bigtalkdp.chapter14;

/**
 * @author chenghz
 * @date 2022/12/28 16:35
 * @description: 游戏角色
 */
public class GameRole {

    /**
     * 生命值
     */
    private int hp;
    /**
     * 攻击力
     */
    private int force;
    /**
     * 等级
     */
    private int level;

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void init() {
        hp = 100;
        force = 100;
        level = 10;
    }

    public void show() {
        System.out.printf("当前生命值%d...%n", hp);
        System.out.printf("当前攻击力%d...%n", force);
        System.out.printf("当前等级%d...%n", level);
    }

    public void fight() {
        System.out.println("开始调整boss...");
        hp = 0;
        force = 0;
        level = 0;
    }

    public RoleMemento createMemento() {
        return new RoleMemento(hp, force, level);
    }

    public void recovery(RoleMemento roleMemento){
        System.out.println("恢复存档...");
        hp = roleMemento.getHp();
        force = roleMemento.getForce();
        level = roleMemento.getLevel();
    }

}
