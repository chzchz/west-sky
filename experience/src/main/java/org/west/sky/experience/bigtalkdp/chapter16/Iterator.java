package org.west.sky.experience.bigtalkdp.chapter16;

/**
 * @author: chz
 * @date: 2023/6/28
 * @description: 迭代器抽象类
 */
public abstract class Iterator<T> {

    public abstract T first();

    public abstract T next();

    public abstract boolean hasNext();

    public abstract T current();
}
