package org.west.sky.experience.bigtalkdp.chapter09;

/**
 * @author chenghz
 * @date 2022/12/6 19:07
 * @description: 指挥者类
 */
public class Director {

    public void Construct(Builder builder){
        builder.buildPartA();
        builder.buildPartB();
    }
}
