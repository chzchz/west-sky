package org.west.sky.experience.bigtalkdp.chapter20;

/**
 * @author: chz
 * @date: 2023/7/14
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        Validator nullValidator = new NullValidator();
        Validator lengthValidator = new LengthValidator();
        Validator patternValidator = new PatternValidator();
        nullValidator.setNextValidator(lengthValidator);
        lengthValidator.setNextValidator(patternValidator);

        nullValidator.execute("123a23123Z23123");
    }
}
