package org.west.sky.experience.bigtalkdp.chapter24;

/**
 * @author: chz
 * @date: 2023/9/25
 * @description:
 */
public interface Action {

    void getManConclusion(Person man);

    void getWomanConclusion(Person woman);
}
