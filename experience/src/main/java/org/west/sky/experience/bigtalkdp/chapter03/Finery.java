package org.west.sky.experience.bigtalkdp.chapter03;

/**
 * @author chenghz
 * @date 2022/11/21 20:03
 * @description: 装饰类基类
 */
public class Finery extends Person {

    private Person component;

    protected void decorate(Person component) {
        this.component = component;
    }

    @Override
    void show() {
        if (component != null) {
            component.show();
        }
    }
}
