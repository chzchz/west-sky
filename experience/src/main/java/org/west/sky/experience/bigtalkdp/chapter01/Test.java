package org.west.sky.experience.bigtalkdp.chapter01;

/**
 * @author chenghz
 * @date 2022/9/12 10:05
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        Operation operation = OperationFactory.createOperation("+");
        operation.setNumberA(12);
        operation.setNumberB(3.0);
        System.out.println(operation.calculate());
    }
}
