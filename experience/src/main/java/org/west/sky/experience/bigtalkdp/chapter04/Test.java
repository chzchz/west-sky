package org.west.sky.experience.bigtalkdp.chapter04;

/**
 * @author chenghz
 * @date 2022/11/22 14:46
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        SchoolGirl jiaojiao = new SchoolGirl();
        jiaojiao.setName("jiaojiao");
        Proxy proxy = new Proxy(jiaojiao);
        proxy.giveDolls();
        proxy.giveFlowers();
        proxy.giveChocolate();
    }
}
