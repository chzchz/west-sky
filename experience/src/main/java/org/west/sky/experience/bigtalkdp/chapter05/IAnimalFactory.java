package org.west.sky.experience.bigtalkdp.chapter05;

/**
 * @author chenghz
 * @date 2022/11/25 15:35
 * @description: 抽象动物工厂
 */
public interface IAnimalFactory {

    /**
     * 创建动物
     *
     * @return
     */
    Animal createAnimal();
}
