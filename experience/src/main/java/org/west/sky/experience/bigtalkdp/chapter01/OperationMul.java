package org.west.sky.experience.bigtalkdp.chapter01;

/**
 * @author chenghz
 * @date 2022/9/12 9:54
 * @description: 乘法操作
 */
public class OperationMul extends Operation{

    @Override
    public double calculate() {
        return getNumberA() * getNumberB();
    }
}
