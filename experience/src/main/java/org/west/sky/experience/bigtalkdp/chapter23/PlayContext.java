package org.west.sky.experience.bigtalkdp.chapter23;

/**
 * @author: chz
 * @date: 2023/9/13
 * @description: 演奏内容
 */
public class PlayContext {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
