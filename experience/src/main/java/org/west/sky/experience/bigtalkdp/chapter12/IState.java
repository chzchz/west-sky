package org.west.sky.experience.bigtalkdp.chapter12;

/**
 * @author chenghz
 * @date 2022/12/28 11:05
 * @description: 抽象状态
 */
public interface IState {

    /**
     * 订单行为
     *
     * @param order
     */
    void handler(Order order);
}
