package org.west.sky.experience.bigtalkdp.chapter21;

/**
 * @author: chz
 * @date: 2023/8/16
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        ClassMonitor classMonitor = new ClassMonitor();

        Teacher teacher = new Teacher(classMonitor);
        Student student = new Student(classMonitor);

        classMonitor.setTeacher(teacher);
        classMonitor.setStudent(student);

        teacher.send("收作业");

        student.send("老师辛苦了");
    }
}
