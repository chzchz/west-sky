package org.west.sky.experience.bigtalkdp.chapter22;

/**
 * @author: chz
 * @date: 2023/8/23
 * @description: 网站用户
 */
public class User {

    private String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
