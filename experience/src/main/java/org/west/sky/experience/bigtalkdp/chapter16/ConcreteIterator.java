package org.west.sky.experience.bigtalkdp.chapter16;

/**
 * @author: chz
 * @date: 2023/6/28
 * @description: 具体的迭代器
 */
public class ConcreteIterator<T> extends Iterator<T> {

    private final Mist<T> mist;

    int current = 0;

    public ConcreteIterator(Mist<T> mist) {
        this.mist = mist;
    }

    @Override
    public T first() {
        return mist.get(0);
    }

    @Override
    public T next() {
        if (current < mist.size()) {
            return mist.get(current++);
        }
        throw new IndexOutOfBoundsException(String.format("expect %d,but only %d", current, mist.size()));
    }

    @Override
    public boolean hasNext() {
        return current < mist.size();
    }

    @Override
    public T current() {
        return mist.get(current);
    }
}
