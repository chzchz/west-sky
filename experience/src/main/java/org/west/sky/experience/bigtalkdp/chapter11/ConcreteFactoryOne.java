package org.west.sky.experience.bigtalkdp.chapter11;

/**
 * @author chenghz
 * @date 2022/12/27 17:48
 * @description: 具体工厂1
 */
public class ConcreteFactoryOne implements IFactory{
    @Override
    public IProductA createA() {
        return new ProductAOne();
    }

    @Override
    public IProductB createB() {
        return new ProductBOne();
    }
}
