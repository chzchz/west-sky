package org.west.sky.experience.bigtalkdp.chapter09;

/**
 * @author chenghz
 * @date 2022/12/6 19:05
 * @description: 具体的建造者1
 */
public class ConcreteBuilderOne extends Builder {

    private Product product = new Product();

    @Override
    public void buildPartA() {
        product.add("部件1......");
    }

    @Override
    public void buildPartB() {
        product.add("部件2......");
    }

    @Override
    public Product getProduct() {
        return product;
    }
}
