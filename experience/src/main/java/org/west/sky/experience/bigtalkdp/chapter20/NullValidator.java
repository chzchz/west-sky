package org.west.sky.experience.bigtalkdp.chapter20;

/**
 * @author: chz
 * @date: 2023/7/14
 * @description: 为空校验
 */
public class NullValidator extends Validator {

    @Override
    public void execute(String str) {
        if (null == str) {
            System.out.println("不能为空");
            return;
        }
        super.nextValidator.execute(str);
    }
}
