package org.west.sky.experience.bigtalkdp.chapter19;

/**
 * @author: chz
 * @date: 2023/7/12
 * @description: 左移控制器
 */
public class Left extends Command {
    @Override
    public void execute() {
        gameCharacter.moveLeft();
    }
}
