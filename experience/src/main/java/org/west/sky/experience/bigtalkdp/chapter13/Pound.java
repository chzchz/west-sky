package org.west.sky.experience.bigtalkdp.chapter13;

/**
 * @author chenghz
 * @date 2022/12/28 15:22
 * @description: 英镑
 */
public class Pound {

    public void usePoundBuy() {
        System.out.println("使用英镑购买...");
    }
}
