package org.west.sky.experience.bigtalkdp.chapter14;

/**
 * @author chenghz
 * @date 2022/12/28 16:39
 * @description: 角色属性存档
 */
public class RoleMemento {

    /**
     * 生命值
     */
    private int hp;
    /**
     * 攻击力
     */
    private int force;
    /**
     * 等级
     */
    private int level;

    public RoleMemento(int hp, int force, int level) {
        this.hp = hp;
        this.force = force;
        this.level = level;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
