package org.west.sky.experience.bigtalkdp.chapter02;

/**
 * @author chenghz
 * @date 2022/10/31 15:25
 * @description:
 */
public class Test {

    public static void main(String[] args) {
//        CashContext cashContext1 = new CashContext("正常收费");
//        CashContext cashContext2 = new CashContext("满100减20");
        CashContext cashContext3 = new CashContext("打8折");

        System.out.println(cashContext3.getResult(100));
    }
}
