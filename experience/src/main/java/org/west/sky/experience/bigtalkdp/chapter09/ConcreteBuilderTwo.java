package org.west.sky.experience.bigtalkdp.chapter09;

/**
 * @author chenghz
 * @date 2022/12/6 19:05
 * @description: 具体的建造者2
 */
public class ConcreteBuilderTwo extends Builder {

    private Product product = new Product();

    @Override
    public void buildPartA() {
        product.add("部件X......");
    }

    @Override
    public void buildPartB() {
        product.add("部件Y......");
    }

    @Override
    public Product getProduct() {
        return product;
    }
}
