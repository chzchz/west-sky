package org.west.sky.experience.bigtalkdp.chapter09;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenghz
 * @date 2022/12/6 19:01
 * @description: 产品
 */
public class Product {

    private List<String> parts = new ArrayList<>();

    public void add(String part) {
        parts.add(part);
    }

    public void show() {
        System.out.println("产品创建....");
        parts.forEach(System.out::println);
    }
}
