package org.west.sky.experience.bigtalkdp.chapter03;

/**
 * @author chenghz
 * @date 2022/11/21 20:06
 * @description: 具体装饰类
 */
public class Shoes extends Finery{

    @Override
    void show() {
        System.out.print("大皮鞋\t");
        super.show();
    }
}
