package org.west.sky.experience.bigtalkdp.chapter07;

/**
 * @author chenghz
 * @date 2022/11/26 17:04
 * @description:
 */
public class ConcreteClassTwo implements Template {

    @Override
    public String answer1() {
        return "2";
    }

    @Override
    public String answer2() {
        return "2";
    }

    @Override
    public String answer3() {
        return "2";
    }
}
