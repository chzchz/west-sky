package org.west.sky.experience.bigtalkdp.chapter21;

/**
 * @author: chz
 * @date: 2023/8/16
 * @description: 具体角色-老师
 */
public class Student extends SchoolRole {
    public Student(Mediator mediator) {
        super(mediator);
    }

    public void send(String message) {
        mediator.send(message, this);
    }

    public void notify(String message) {
        System.out.println("学生收到消息:" + message);
    }
}
