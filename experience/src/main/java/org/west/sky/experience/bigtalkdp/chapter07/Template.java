package org.west.sky.experience.bigtalkdp.chapter07;

/**
 * @author chenghz
 * @date 2022/11/26 17:01
 * @description: 模板方法
 */
public interface Template {

    /**
     * 骨架方法
     */
    default void doWork() {
        System.out.println("1+1=" + answer1());
        System.out.println("1+2=" + answer2());
        System.out.println("1+3=" + answer3());
    }

    /**
     * 方法1
     *
     * @return
     */
    default String answer1() {
        return "2";
    }

    /**
     * 方法2
     *
     * @return
     */
    default String answer2() {
        return "3";
    }

    /**
     * 方法1
     *
     * @return
     */
    default String answer3() {
        return "4";
    }
}
