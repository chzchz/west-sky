package org.west.sky.experience.bigtalkdp.chapter11;

/**
 * @author chenghz
 * @date 2022/12/27 17:49
 * @description: 具体产品A2
 */
public class ProductATwo implements IProductA{
    public ProductATwo() {
        System.out.println("具体产品A2被创建....");
    }
}
