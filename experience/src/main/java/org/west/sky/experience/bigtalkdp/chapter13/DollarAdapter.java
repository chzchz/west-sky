package org.west.sky.experience.bigtalkdp.chapter13;

/**
 * @author chenghz
 * @date 2022/12/28 15:22
 * @description: 美元适配器
 */
public class DollarAdapter implements IMoney {

    private final Dollar dollar = new Dollar();

    @Override
    public void buy() {
        System.out.println("100人民币=1000美元");
        dollar.useDollarBuy();
    }
}
