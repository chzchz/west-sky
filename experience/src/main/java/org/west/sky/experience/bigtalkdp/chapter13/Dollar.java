package org.west.sky.experience.bigtalkdp.chapter13;

/**
 * @author chenghz
 * @date 2022/12/28 15:22
 * @description: 美元
 */
public class Dollar {

    public void useDollarBuy() {
        System.out.println("使用美元购买...");
    }
}
