package org.west.sky.experience.bigtalkdp.chapter14;

/**
 * @author chenghz
 * @date 2022/12/28 16:40
 * @description: 角色存档管理
 */
public class RoleCaretaker {

    private RoleMemento roleMemento;

    public RoleMemento getRoleMemento() {
        return roleMemento;
    }

    public void setRoleMemento(RoleMemento roleMemento) {
        this.roleMemento = roleMemento;
    }
}
