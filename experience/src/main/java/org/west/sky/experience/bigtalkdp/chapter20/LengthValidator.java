package org.west.sky.experience.bigtalkdp.chapter20;

/**
 * @author: chz
 * @date: 2023/7/14
 * @description: 长度校验
 */
public class LengthValidator extends Validator {

    @Override
    public void execute(String str) {
        if (str.length() == 0 || str.length() > 20) {
            System.out.println("长度不能为0且不能超过20个字符");
            return;
        }
        super.nextValidator.execute(str);
    }
}
