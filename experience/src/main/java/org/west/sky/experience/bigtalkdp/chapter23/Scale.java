package org.west.sky.experience.bigtalkdp.chapter23;

/**
 * @author: chz
 * @date: 2023/9/13
 * @description: 音阶解释器
 */
public class Scale extends Expression {

    @Override
    public void execute(String key, double value) {
        String scale = "";

        switch (Double.valueOf(value).intValue()) {
            case 1:
                scale = "低音";
                break;
            case 2:
                scale = "中音";
                break;
            case 3:
                scale = "高音";
                break;
        }

        System.out.printf(scale + " ");
    }
}
