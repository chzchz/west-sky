package org.west.sky.experience.bigtalkdp.chapter02;

/**
 * @author chenghz
 * @date 2022/10/31 15:02
 * @description: 抽象算法
 */
public abstract class CashSuper {

    /**
     * 计算
     *
     * @param money
     * @return
     */
    public abstract double acceptCash(double money);
}
