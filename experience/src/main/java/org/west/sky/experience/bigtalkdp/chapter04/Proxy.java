package org.west.sky.experience.bigtalkdp.chapter04;

/**
 * @author chenghz
 * @date 2022/11/22 14:44
 * @description: 代理
 */
public class Proxy implements GiveGift{

    private Pursuit pursuit;

    public Proxy(SchoolGirl schoolGirl) {
        this.pursuit = new Pursuit(schoolGirl);
    }

    @Override
    public void giveDolls() {
        pursuit.giveDolls();
    }

    @Override
    public void giveFlowers() {
        pursuit.giveFlowers();
    }

    @Override
    public void giveChocolate() {
        pursuit.giveChocolate();
    }
}
