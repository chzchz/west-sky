package org.west.sky.experience.bigtalkdp.chapter11;

/**
 * @author chenghz
 * @date 2022/12/27 17:49
 * @description: 具体产品A1
 */
public class ProductAOne implements IProductA {

    public ProductAOne() {
        System.out.println("具体产品A1被创建....");
    }
}
