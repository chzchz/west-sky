package org.west.sky.experience.bigtalkdp.chapter11;

/**
 * @author chenghz
 * @date 2022/12/27 17:44
 * @description: 抽象工厂
 */
public interface IFactory {
    /**
     * 创建产品A
     *
     * @return
     */
    IProductA createA();

    /**
     * 创建产品B
     *
     * @return
     */
    IProductB createB();
}
