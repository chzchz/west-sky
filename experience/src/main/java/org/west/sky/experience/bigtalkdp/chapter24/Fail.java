package org.west.sky.experience.bigtalkdp.chapter24;

/**
 * @author: chz
 * @date: 2023/9/25
 * @description:
 */
public class Fail implements Action {

    private String action = "失败";

    @Override
    public void getManConclusion(Person man) {
        System.out.printf("%s%s时,xxxxxxxxxxx%n", man.getType(), action);
    }

    @Override
    public void getWomanConclusion(Person woman) {
        System.out.printf("%s%s时,xxxxxxxxxxx%n", woman.getType(), action);
    }
}
