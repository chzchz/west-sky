package org.west.sky.experience.bigtalkdp.chapter21;

/**
 * @author: chz
 * @date: 2023/8/16
 * @description: 抽象中介者
 */
public abstract class Mediator {

    public abstract void send(String message, SchoolRole role);
}
