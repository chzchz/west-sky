package org.west.sky.experience.bigtalkdp.chapter09;

/**
 * @author chenghz
 * @date 2022/12/6 19:00
 * @description: 抽象建造者
 */
public abstract class Builder {

    /**
     * 第一部分
     */
    public abstract void buildPartA();

    /**
     * 第二部分
     */
    public abstract void buildPartB();

    /**
     * 获取建造对象
     *
     * @return
     */
    public abstract Product getProduct();

}
