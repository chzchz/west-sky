package org.west.sky.experience.bigtalkdp.chapter19;

/**
 * @author: chz
 * @date: 2023/7/12
 * @description: 游戏角色
 */
public class GameCharacter {

    public void moveLeft() {
        System.out.println("向左走一步");
    }

    public void moveRight() {
        System.out.println("向右走一步");
    }

    public void moveUp() {
        System.out.println("向上走一步");
    }

    public void moveDown() {
        System.out.println("向下走一步");
    }
}
