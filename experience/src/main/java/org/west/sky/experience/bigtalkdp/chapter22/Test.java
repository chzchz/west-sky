package org.west.sky.experience.bigtalkdp.chapter22;

/**
 * @author: chz
 * @date: 2023/8/23
 * @description:
 */
public class Test {

    public static void main(String[] args) {

        WebsiteFactory factory = new WebsiteFactory();

        Website blog = factory.createWebsite("博客");
        blog.use(new User("小李"));

        Website blog2 = factory.createWebsite("博客");
        blog2.use(new User("小张"));

        Website blog3 = factory.createWebsite("博客");
        blog2.use(new User("小吴"));

        Website facebook = factory.createWebsite("脸书");
        facebook.use(new User("小赵"));

        Website facebook2 = factory.createWebsite("脸书");
        facebook2.use(new User("小孙"));

        Website facebook3 = factory.createWebsite("脸书");
        facebook3.use(new User("小王"));

        System.out.println("享元对象数量:" + factory.count());
    }
}
