package org.west.sky.experience.bigtalkdp.chapter06;

/**
 * @author chenghz
 * @date 2022/11/25 16:34
 * @description:
 */
public class Grade implements Cloneable{

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Grade clone() throws CloneNotSupportedException {
        return (Grade) super.clone();
    }

    @Override
    public String toString() {
        return "Grade{" +
                "name='" + name + '\'' +
                '}';
    }
}
