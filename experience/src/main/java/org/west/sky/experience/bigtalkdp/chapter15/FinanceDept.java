package org.west.sky.experience.bigtalkdp.chapter15;

/**
 * @author chenghz
 * @date 2022/12/28 17:51
 * @description: 财务部
 */
public class FinanceDept extends Company {

    public FinanceDept(String name) {
        super(name);
    }

    @Override
    public void add(Company company) {

    }

    @Override
    public void remove(Company company) {

    }

    @Override
    public void display(int depth) {
        System.out.println(new String(new char[depth]).replaceAll("\0", "-") + name);
    }

    @Override
    public void lineOfDuty() {
        System.out.printf("%s 公司财务收支管理%n", name);
    }
}
