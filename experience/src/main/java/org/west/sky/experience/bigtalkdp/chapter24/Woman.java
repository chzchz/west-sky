package org.west.sky.experience.bigtalkdp.chapter24;

/**
 * @author: chz
 * @date: 2023/9/25
 * @description:
 */
public class Woman extends Person{
    @Override
    public String getType() {
        return "女人";
    }

    @Override
    public void accept(Action action) {
        action.getWomanConclusion(this);
    }
}
