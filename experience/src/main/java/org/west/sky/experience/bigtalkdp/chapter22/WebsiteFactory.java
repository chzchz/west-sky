package org.west.sky.experience.bigtalkdp.chapter22;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: chz
 * @date: 2023/8/23
 * @description: 网站工厂
 */
public class WebsiteFactory {

    private final Map<String, Website> flyweights = new HashMap<>();

    public Website createWebsite(String key) {
        if (flyweights.containsKey(key)) {
            return flyweights.get(key);
        }
        Website website = new ConcreteWebsite(key);
        flyweights.put(key, website);
        return website;
    }

    public int count() {
        return flyweights.size();
    }
}
