package org.west.sky.experience.bigtalkdp.chapter22;

/**
 * @author: chz
 * @date: 2023/8/23
 * @description: 网站抽象类
 */
public abstract class Website {

    protected String name;

    public Website(String name) {
        this.name = name;
    }

    public abstract void use(User user);
}
