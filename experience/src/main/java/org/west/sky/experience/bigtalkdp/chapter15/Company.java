package org.west.sky.experience.bigtalkdp.chapter15;

/**
 * @author chenghz
 * @date 2022/12/28 17:48
 * @description: 抽象公司
 */
public abstract class Company {

    /**
     * 公司名字
     */
    protected String name;

    public Company(String name) {
        this.name = name;
    }

    /**
     * 添加
     *
     * @param company
     */
    public abstract void add(Company company);

    /**
     * 删除
     *
     * @param company
     */
    public abstract void remove(Company company);

    /**
     * 展示
     *
     * @param depth
     */
    public abstract void display(int depth);

    /**
     * 履行职责
     */
    public abstract void lineOfDuty();
}
