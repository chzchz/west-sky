package org.west.sky.experience.bigtalkdp.chapter10;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenghz
 * @date 2022/12/26 16:17
 * @description: 抽象通知者
 */
public abstract class Subject {
    /**
     * 观察者集合
     */
    private final List<Observer> observers = new ArrayList<>();

    /**
     * 通知者状态
     */
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void attach(Observer observer) {
        observers.add(observer);
    }

    public void del(Observer observer) {
        observers.remove(observer);
    }

    public void notice() {
        observers.forEach(Observer::update);
    }

}
