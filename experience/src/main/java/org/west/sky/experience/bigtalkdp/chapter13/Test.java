package org.west.sky.experience.bigtalkdp.chapter13;

/**
 * @author chenghz
 * @date 2022/12/28 15:27
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        IMoney dollar = new DollarAdapter();
        dollar.buy();

        IMoney pound = new PoundAdapter();
        pound.buy();
    }
}
