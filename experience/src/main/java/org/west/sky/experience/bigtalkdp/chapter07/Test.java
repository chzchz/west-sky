package org.west.sky.experience.bigtalkdp.chapter07;

/**
 * @author chenghz
 * @date 2022/11/26 17:05
 * @description:
 */
public class Test {

    public static void main(String[] args) {

        Template templateOne = new ConcreteClassOne();
        Template templateTwo = new ConcreteClassTwo();

        Template templateThree = new Template() {};

        templateOne.doWork();
        templateTwo.doWork();
        templateThree.doWork();

    }
}
