package org.west.sky.experience.bigtalkdp.chapter12;

/**
 * @author chenghz
 * @date 2022/12/28 11:11
 * @description:
 */
public class EndState implements IState {

    @Override
    public void handler(Order order) {
        System.out.printf("当前已经下单超过%s天，已结束%n", order.getDays());
    }
}
