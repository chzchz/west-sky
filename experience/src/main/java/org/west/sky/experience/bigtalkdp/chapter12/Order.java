package org.west.sky.experience.bigtalkdp.chapter12;

/**
 * @author chenghz
 * @date 2022/12/28 11:06
 * @description: 订单
 */
public class Order {

    /**
     * 订单状态
     */
    private IState orderState;
    /**
     * 下单天数
     */
    private int days;

    public Order() {
        this.orderState = new OutBoundState();
    }

    public IState getOrderState() {
        return orderState;
    }

    public void setOrderState(IState orderState) {
        this.orderState = orderState;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public void process() {
        this.orderState.handler(this);
    }
}
