package org.west.sky.experience.bigtalkdp.chapter12;

/**
 * @author chenghz
 * @date 2022/12/28 11:22
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        Order order = new Order();
        order.setDays(1);
        order.process();
        order.setDays(2);
        order.process();
        order.setDays(3);
        order.process();
        order.setDays(5);
        order.process();
        order.setDays(10);
        order.process();
        order.setDays(15);
        order.process();
        order.setDays(20);
        order.process();
    }
}
