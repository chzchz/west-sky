package org.west.sky.experience.bigtalkdp.chapter15;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenghz
 * @date 2022/12/28 17:59
 * @description: 具体的子公司
 */
public class ConcreteCompany extends Company {

    private List<Company> companyList = new ArrayList<>();

    public ConcreteCompany(String name) {
        super(name);
    }

    @Override
    public void add(Company company) {
        companyList.add(company);
    }

    @Override
    public void remove(Company company) {
        companyList.remove(company);
    }

    @Override
    public void display(int depth) {
        System.out.println(new String(new char[depth]).replaceAll("\0", "-") + name);
        companyList.forEach(l -> l.display(depth + 2));
    }

    @Override
    public void lineOfDuty() {
        companyList.forEach(Company::lineOfDuty);
    }
}
