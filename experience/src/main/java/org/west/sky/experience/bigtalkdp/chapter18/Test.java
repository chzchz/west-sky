package org.west.sky.experience.bigtalkdp.chapter18;

/**
 * @author: chz
 * @date: 2023/7/12
 * @description:
 */
public class Test {
    public static void main(String[] args) {
        PhoneBrand mi = new MiPhone();
        PhoneBrand apple = new ApplePhone();

        Soft game = new GameSoft();
        Soft music = new MusicSoft();

        mi.setSoft(game);
        mi.play();

        mi.setSoft(music);
        mi.play();

        apple.setSoft(game);
        apple.play();

        apple.setSoft(music);
        apple.play();
    }
}
