package org.west.sky.experience.bigtalkdp.chapter05;

/**
 * @author chenghz
 * @date 2022/11/25 15:30
 * @description: 抽象动物类
 */
public class Animal {

    /**
     * 吃饭
     */
    public void eat() {
        System.out.println("吃饭");
    }

    /**
     * 睡觉
     */
    public void sleep() {
        System.out.println("睡觉");
    }

    /**
     * 打豆豆
     */
    public void play() {
        System.out.println("打豆豆");
    }
}
