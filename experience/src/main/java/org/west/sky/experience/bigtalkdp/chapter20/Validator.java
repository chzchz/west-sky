package org.west.sky.experience.bigtalkdp.chapter20;

/**
 * 校验器
 */
public abstract class Validator {

    protected Validator nextValidator;

    public void setNextValidator(Validator nextValidator) {
        this.nextValidator = nextValidator;
    }

    /**
     * 执行校验逻辑
     */
    public abstract void execute(String str);
}
