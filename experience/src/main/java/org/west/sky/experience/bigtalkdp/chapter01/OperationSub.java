package org.west.sky.experience.bigtalkdp.chapter01;

/**
 * @author chenghz
 * @date 2022/9/12 9:53
 * @description: 减法操作
 */
public class OperationSub extends Operation{

    @Override
    public double calculate() {
        return getNumberA() - getNumberB();
    }
}
