package org.west.sky.experience.bigtalkdp.chapter02;

/**
 * @author chenghz
 * @date 2022/10/31 15:10
 * @description: 打折
 */
public class CashRebate extends CashSuper {

    private double rebate;

    public CashRebate(double rebate) {
        this.rebate = rebate;
    }

    @Override
    public double acceptCash(double money) {
        System.out.printf("打%f折", this.rebate);
        return money * this.rebate;
    }
}
