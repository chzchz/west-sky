package org.west.sky.experience.bigtalkdp.chapter10;

/**
 * @author chenghz
 * @date 2022/12/26 16:35
 * @description: 具体的观察者
 */
public class ConcreteObserver implements Observer {


    /**
     * 姓名
     */
    private String name;

    private Subject subject;

    public ConcreteObserver(String name, Subject subject) {
        this.name = name;
        this.subject = subject;
    }

    @Override
    public void update() {
        System.out.println(String.format("%s观察到的状态是%s", name, subject.getStatus()));
    }
}
