package org.west.sky.experience.bigtalkdp.chapter06;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenghz
 * @date 2022/11/25 16:34
 * @description: 学校
 */
public class School implements Cloneable{

    private String name;

    private String type;

    private List<Grade> grades;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Grade> getGrades() {
        return grades;
    }

    public void setGrades(List<Grade> grades) {
        this.grades = grades;
    }

    @Override
    public String toString() {
        return "School{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", grades=" + grades +
                '}';
    }

    @Override
    protected School clone() throws CloneNotSupportedException {
        School school = (School) super.clone();

        //即使Grade也实现了clone方法，也没用，还是直接指向同一个对象，必须对内部对象进行clone才能实现深拷贝
        List<Grade> grades = school.getGrades();
        List<Grade> cloneGrades = new ArrayList<>();
        grades.forEach(e-> {
            try {
                cloneGrades.add(e.clone());
            } catch (CloneNotSupportedException cloneNotSupportedException) {
                cloneNotSupportedException.printStackTrace();
            }
        });
        school.setGrades(cloneGrades);
        return school;
    }
}
