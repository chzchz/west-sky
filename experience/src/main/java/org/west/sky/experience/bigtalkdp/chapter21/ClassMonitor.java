package org.west.sky.experience.bigtalkdp.chapter21;

/**
 * @author: chz
 * @date: 2023/8/16
 * @description: 具体中介者--班长
 */
public class ClassMonitor extends Mediator{

    private Teacher teacher;

    private Student student;

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public void send(String message, SchoolRole role) {
        if(role == teacher){
            student.notify(message);
        }
        if(role == student){
            teacher.notify(message);
        }
    }
}
