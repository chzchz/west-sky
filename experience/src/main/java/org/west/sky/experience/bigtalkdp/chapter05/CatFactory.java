package org.west.sky.experience.bigtalkdp.chapter05;

/**
 * @author chenghz
 * @date 2022/11/25 15:36
 * @description: 猫工厂
 */
public class CatFactory implements IAnimalFactory{

    @Override
    public Animal createAnimal() {
        return new Cat();
    }
}
