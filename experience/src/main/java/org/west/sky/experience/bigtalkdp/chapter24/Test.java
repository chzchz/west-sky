package org.west.sky.experience.bigtalkdp.chapter24;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: chz
 * @date: 2023/9/25
 * @description:
 */
public class Test {

    public static void main(String[] args) {

        Action success = new Success();
        Action fail = new Fail();
        Man man1 = new Man();
        man1.accept(success);
        man1.accept(fail);
        Person woman1 = new Woman();
        woman1.accept(success);
        woman1.accept(fail);


    }
}
