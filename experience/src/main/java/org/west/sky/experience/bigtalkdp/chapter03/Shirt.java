package org.west.sky.experience.bigtalkdp.chapter03;

/**
 * @author chenghz
 * @date 2022/11/21 20:06
 * @description: 具体装饰类
 */
public class Shirt extends Finery{

    @Override
    void show() {
        System.out.print("大T恤\t");
        super.show();
    }
}
