package org.west.sky.experience.bigtalkdp.chapter16;

/**
 * @author: chz
 * @date: 2023/6/28
 * @description: 被迭代抽象类
 */
public interface Mist<T> {

    Iterator<T> createIterator();

    Iterator<T> createIteratorDesc();

    T get(int index);

    void add(T t);

    int size();
}
