package org.west.sky.experience.bigtalkdp.chapter01;

/**
 * @author chenghz
 * @date 2022/9/12 9:51
 * @description: 加法操作
 */
public class OperationAdd extends Operation{

    @Override
    public double calculate() {
        return getNumberA() + getNumberB();
    }
}
