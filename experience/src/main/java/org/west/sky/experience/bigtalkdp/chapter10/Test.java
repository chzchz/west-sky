package org.west.sky.experience.bigtalkdp.chapter10;

/**
 * @author chenghz
 * @date 2022/12/26 16:39
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        Subject subject = new ConcreteSubject();
        Observer observer1 = new ConcreteObserver("zhangsan", subject);
        Observer observer2 = new ConcreteObserver("lisi", subject);
        subject.attach(observer1);
        subject.attach(observer2);
        subject.setStatus("开机");
        subject.notice();
    }
}
