package org.west.sky.experience.bigtalkdp.chapter06;

import java.util.Arrays;

/**
 * @author chenghz
 * @date 2022/11/25 16:36
 * @description:
 */
public class Test {

    public static void main(String[] args) throws CloneNotSupportedException {

        Grade grade = new Grade();
        grade.setName("高一年级");

        Grade grade2 = grade.clone();
        grade2.setName("高二年级");

        System.out.println(grade);
        System.out.println(grade2);

        School school = new School();
        school.setName("测试一中");
        school.setType("高中");
        school.setGrades(Arrays.asList(grade, grade2));

        System.out.println(school);

        School school2 = school.clone();
        school2.setName("测试二中");
        school2.setType("初中");
        school2.getGrades().get(0).setName("初一年级");
        school2.getGrades().get(1).setName("初二年级");
        System.out.println(school2);
        //java的clone是浅拷贝，详情见School的clone方法
        System.out.println(school);
    }
}
