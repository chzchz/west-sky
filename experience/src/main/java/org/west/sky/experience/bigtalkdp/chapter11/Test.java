package org.west.sky.experience.bigtalkdp.chapter11;

/**
 * @author chenghz
 * @date 2022/12/27 17:52
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        IFactory factoryOne = new ConcreteFactoryOne();
        factoryOne.createA();
        factoryOne.createB();

        IFactory factoryTwo = new ConcreteFactoryTwo();
        factoryTwo.createA();
        factoryTwo.createB();
    }
}
