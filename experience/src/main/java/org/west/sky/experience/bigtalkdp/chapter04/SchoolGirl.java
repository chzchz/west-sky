package org.west.sky.experience.bigtalkdp.chapter04;

/**
 * @author chenghz
 * @date 2022/11/22 14:33
 * @description:
 */
public class SchoolGirl {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
