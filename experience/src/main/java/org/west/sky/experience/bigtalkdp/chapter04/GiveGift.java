package org.west.sky.experience.bigtalkdp.chapter04;

/**
 * @author chenghz
 * @date 2022/11/22 14:21
 * @description: 公共方法抽象类
 */
public interface GiveGift {

    /**
     * 送娃娃
     */
    void giveDolls();

    /**
     * 送花
     */
    void giveFlowers();

    /**
     * 送巧克力
     */
    void giveChocolate();
}
