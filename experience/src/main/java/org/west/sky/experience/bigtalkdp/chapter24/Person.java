package org.west.sky.experience.bigtalkdp.chapter24;

/**
 * @author: chz
 * @date: 2023/9/25
 * @description:
 */
public abstract class Person {

    public abstract String getType();

    public abstract void accept(Action action);
}
