package org.west.sky.experience.bigtalkdp.chapter15;

/**
 * @author chenghz
 * @date 2022/12/28 18:02
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        ConcreteCompany root = new ConcreteCompany("北京总公司");
        root.add(new HrDept("总公司人力资源部"));
        root.add(new FinanceDept("总公司财务部"));

        ConcreteCompany fengongsi = new ConcreteCompany("上海华东分公司");
        fengongsi.add(new HrDept("上海华东分公司人力资源部"));
        fengongsi.add(new FinanceDept("上海华东分公司财务部"));
        ConcreteCompany nanjing = new ConcreteCompany("南京办事处");
        nanjing.add(new HrDept("南京办事处人力资源部"));
        nanjing.add(new FinanceDept("南京办事处财务部"));
        ConcreteCompany hangzhou = new ConcreteCompany("杭州办事处");
        hangzhou.add(new HrDept("杭州办事处人力资源部"));
        hangzhou.add(new FinanceDept("杭州办事处财务部"));
        fengongsi.add(nanjing);
        fengongsi.add(hangzhou);
        root.add(fengongsi);

        System.out.println("公司架构图");

        root.display(1);

        root.lineOfDuty();

    }
}
