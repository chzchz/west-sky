package org.west.sky.experience.bigtalkdp.chapter12;

/**
 * @author chenghz
 * @date 2022/12/28 11:11
 * @description:
 */
public class OutBoundState implements IState {

    @Override
    public void handler(Order order) {
        if (order.getDays() < 2) {
            System.out.printf("当前已经下单%s天，订单出库中...%n", order.getDays());
        }else {
            order.setOrderState(new SendState());
            order.process();
        }
    }
}
