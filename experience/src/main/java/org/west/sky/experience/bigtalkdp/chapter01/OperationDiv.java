package org.west.sky.experience.bigtalkdp.chapter01;

/**
 * @author chenghz
 * @date 2022/9/12 9:55
 * @description: 除法操作
 */
public class OperationDiv extends Operation {

    @Override
    public double calculate() {
        if (0 == getNumberB()) {
            throw new RuntimeException("除数不能为0");
        }
        return getNumberA() / getNumberB();
    }
}
