package org.west.sky.experience.bigtalkdp.chapter01;

/**
 * @author chenghz
 * @date 2022/9/12 9:50
 * @description: 定义操作
 */
public class Operation {

    private double numberA;

    private double numberB;

    public double getNumberA() {
        return numberA;
    }

    public void setNumberA(double numberA) {
        this.numberA = numberA;
    }

    public double getNumberB() {
        return numberB;
    }

    public void setNumberB(double numberB) {
        this.numberB = numberB;
    }

    public double calculate() {
        return 0;
    }
}
