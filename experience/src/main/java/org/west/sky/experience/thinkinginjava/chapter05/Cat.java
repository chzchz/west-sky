package org.west.sky.experience.thinkinginjava.chapter05;

/**
 * @Author: chz
 * @Description: 静态成员初始化
 * 顺序：静态成员>非静态成员>构造方法
 * @Date: 2023/3/9
 */
public class Cat {

    String name;

    public Cat(String name){
        System.out.println("i am a cat, name is " + name);
    }
}
