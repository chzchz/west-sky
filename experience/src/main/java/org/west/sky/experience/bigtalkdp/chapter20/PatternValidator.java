package org.west.sky.experience.bigtalkdp.chapter20;

import java.util.regex.Pattern;

/**
 * @author: chz
 * @date: 2023/7/14
 * @description: 格式校验
 */
public class PatternValidator extends Validator {


    @Override
    public void execute(String str) {
        //假的正则
        String pattern = "^[a-z]+[A-Z]+\\d+";
        if (!Pattern.matches(pattern, str)) {
            System.out.println("必须同时包含数字和大小写字母");
        }
    }
}
