package org.west.sky.experience.bigtalkdp.chapter19;

/**
 * @author: chz
 * @date: 2023/7/12
 * @description:
 */
public class Up extends Command {
    @Override
    public void execute() {
        gameCharacter.moveUp();
    }
}
