package org.west.sky.experience.bigtalkdp.chapter24;

/**
 * @author: chz
 * @date: 2023/9/25
 * @description:
 */
public class Man extends Person{
    @Override
    public String getType() {
        return "男人";
    }

    @Override
    public void accept(Action action) {
        action.getManConclusion(this);
    }
}
