package org.west.sky.experience.bigtalkdp.chapter05;

/**
 * @author chenghz
 * @date 2022/11/25 15:37
 * @description:
 */
public class DogFactory implements IAnimalFactory {

    @Override
    public Animal createAnimal() {
        return new Dog();
    }
}
