package org.west.sky.experience.io.aio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

/**
 * author: chz
 * date: 2024/2/4
 * description:
 */
public class AioClient {

    public static void main(String[] args) throws IOException, InterruptedException {
        //创建异步socket通道
        AsynchronousSocketChannel channel = AsynchronousSocketChannel.open();
        //异步连接服务器
        channel.connect(new java.net.InetSocketAddress("127.0.0.1", 4444), null, new CompletionHandler<Void, Object>() {
            final ByteBuffer byteBuffer = ByteBuffer.wrap("hello server".getBytes());

            //异步读取服务器发送的消息
            @Override
            public void completed(Void result, Object attachment) {
                //异步发送消息给服务器
                channel.write(byteBuffer, null, new CompletionHandler<Integer, Object>() {
                    final ByteBuffer readBuffer = ByteBuffer.allocate(1024);

                    @Override
                    public void completed(Integer result, Object attachment) {
                        readBuffer.clear();
                        //异步读取服务器发送的信息
                        channel.read(readBuffer, null, new CompletionHandler<Integer, Object>() {
                            @Override
                            public void completed(Integer result, Object attachment) {
                                readBuffer.flip();
                                System.out.println("接收到服务消息:" + new String(readBuffer.array(), 0, result));
                            }

                            @Override
                            public void failed(Throwable exc, Object attachment) {
                                exc.printStackTrace();
                                try {
                                    channel.close();
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });
                    }

                    @Override
                    public void failed(Throwable exc, Object attachment) {
                        exc.printStackTrace();
                        try {
                            channel.close();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });

            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                exc.printStackTrace();
                try {
                    channel.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        //等待连接处理完毕
        Thread.sleep(1000);
        channel.shutdownInput();
        channel.close();
    }
}
