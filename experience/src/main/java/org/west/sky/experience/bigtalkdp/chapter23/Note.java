package org.west.sky.experience.bigtalkdp.chapter23;

/**
 * @author: chz
 * @date: 2023/9/13
 * @description: 音符解释器
 */
public class Note extends Expression {
    @Override
    public void execute(String key, double value) {
        String note = "";
        switch (key) {
            case "C":
                note = "Do";
                break;
            case "D":
                note = "Ri";
                break;
            case "E":
                note = "Mi";
                break;
            case "F":
                note = "Fa";
                break;
            case "G":
                note = "So";
                break;
            case "A":
                note = "La";
                break;
            case "B":
                note = "Ti";
                break;
        }
        System.out.print(note + " ");
    }
}
