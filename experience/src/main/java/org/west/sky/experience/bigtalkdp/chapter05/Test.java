package org.west.sky.experience.bigtalkdp.chapter05;

/**
 * @author chenghz
 * @date 2022/11/25 15:52
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        IAnimalFactory catFactory = new CatFactory();
        IAnimalFactory dogFactory = new DogFactory();

        Animal cat1 = catFactory.createAnimal();
        Animal cat2 = catFactory.createAnimal();
        Animal cat3 = catFactory.createAnimal();

        Animal dog1 = dogFactory.createAnimal();
        Animal dog2 = dogFactory.createAnimal();
        Animal dog3 = dogFactory.createAnimal();

        cat1.eat();
        cat1.sleep();
        cat1.play();

        dog1.eat();
        dog1.sleep();
        dog1.play();
    }
}
