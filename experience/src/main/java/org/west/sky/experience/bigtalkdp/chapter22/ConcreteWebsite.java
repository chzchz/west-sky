package org.west.sky.experience.bigtalkdp.chapter22;

/**
 * @author: chz
 * @date: 2023/8/23
 * @description: 具体的网站类
 */
public class ConcreteWebsite extends Website {

    public ConcreteWebsite(String name) {
        super(name);
    }

    @Override
    public void use(User user) {
        System.out.println("网站分类：" + super.name + " 用户：" + user.getName());
    }
}
