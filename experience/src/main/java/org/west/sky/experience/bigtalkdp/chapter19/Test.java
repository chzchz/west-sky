package org.west.sky.experience.bigtalkdp.chapter19;

/**
 * @author: chz
 * @date: 2023/7/12
 * @description:
 */
public class Test {

    public static void main(String[] args) {

        GameCharacter character = new GameCharacter();

        Controller controller = new Controller();

        Command left = new Left();
        left.setGameCharacter(character);
        controller.setCommand(left);
        controller.sendCommand();

        Command right = new Right();
        right.setGameCharacter(character);
        controller.setCommand(right);
        controller.sendCommand();

        Command up = new Up();
        up.setGameCharacter(character);
        controller.setCommand(up);
        controller.sendCommand();

        Command down = new Down();
        down.setGameCharacter(character);
        controller.setCommand(down);
        controller.sendCommand();
    }
}
