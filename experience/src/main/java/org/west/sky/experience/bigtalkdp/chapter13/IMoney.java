package org.west.sky.experience.bigtalkdp.chapter13;

/**
 * @author chenghz
 * @date 2022/12/28 15:21
 * @description: 抽象
 */
public interface IMoney {
    /**
     * 购买东西
     */
    void buy();
}
