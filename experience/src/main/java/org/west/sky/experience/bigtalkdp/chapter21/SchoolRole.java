package org.west.sky.experience.bigtalkdp.chapter21;

/**
 * @author: chz
 * @date: 2023/8/16
 * @description: 抽象学校人员角色
 */
public abstract class SchoolRole {

    protected Mediator mediator;

    public SchoolRole(Mediator mediator) {
        this.mediator = mediator;
    }

    public abstract void send(String message);
}
