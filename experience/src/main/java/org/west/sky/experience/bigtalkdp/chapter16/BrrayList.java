package org.west.sky.experience.bigtalkdp.chapter16;

/**
 * @author: chz
 * @date: 2023/6/28
 * @description:
 */
public class BrrayList<T> implements Mist<T> {

    private final Object[] arr;

    private static final int DEFAULT_CAPACITY = 10;

    int cursor = 0;

    int capacity;

    public BrrayList() {
        this(DEFAULT_CAPACITY);
    }

    public BrrayList(int size) {
        arr = new Object[size];
        this.capacity = size;
    }

    @Override
    public Iterator<T> createIterator() {
        return new ConcreteIterator<>(this);
    }

    @Override
    public Iterator<T> createIteratorDesc() {
        return new ConcreteIteratorDesc<>(this);
    }

    @Override
    public T get(int index) {
        return (T) arr[index];
    }

    @Override
    public void add(T t) {
        if (cursor + 1 < capacity) {
            arr[cursor++] = t;
        }
    }

    @Override
    public int size() {
        return cursor;
    }
}
