package org.west.sky.experience.bigtalkdp.chapter12;

/**
 * @author chenghz
 * @date 2022/12/28 11:11
 * @description:
 */
public class ReceiveState implements IState {

    @Override
    public void handler(Order order) {
        if (order.getDays() < 10) {
            System.out.printf("当前已经下单%s天，订单签收中...%n", order.getDays());
        }else {
            order.setOrderState(new EvaluateState());
            order.process();
        }
    }
}
