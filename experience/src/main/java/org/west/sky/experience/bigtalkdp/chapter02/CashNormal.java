package org.west.sky.experience.bigtalkdp.chapter02;

/**
 * @author chenghz
 * @date 2022/10/31 15:09
 * @description:
 */
public class CashNormal extends CashSuper {

    @Override
    public double acceptCash(double money) {
        System.out.println("正常计算");
        return money;
    }
}
