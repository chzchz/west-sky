package org.west.sky.experience.bigtalkdp.chapter02;

/**
 * @author chenghz
 * @date 2022/10/31 15:12
 * @description: 满减
 */
public class CashReturn extends CashSuper {

    private double threshold;

    private double quota;

    public CashReturn(double threshold, double quota) {
        this.threshold = threshold;
        this.quota = quota;
    }

    @Override
    public double acceptCash(double money) {
        System.out.printf("满%f减%f", threshold, quota);
        System.out.println();
        return money - (money / threshold) * quota;
    }
}
