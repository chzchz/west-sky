package org.west.sky.experience.bigtalkdp.chapter14;

/**
 * @author chenghz
 * @date 2022/12/28 16:43
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        GameRole maliao = new GameRole();
        maliao.init();
        maliao.show();

        RoleCaretaker roleCaretaker = new RoleCaretaker();
        roleCaretaker.setRoleMemento(maliao.createMemento());

        maliao.fight();
        maliao.show();

        maliao.recovery(roleCaretaker.getRoleMemento());
        maliao.show();
    }
}
