package org.west.sky.experience.bigtalkdp.chapter19;

/**
 * @author: chz
 * @date: 2023/7/12
 * @description: 下移控制器
 */
public class Down extends Command {
    @Override
    public void execute() {
        gameCharacter.moveDown();
    }
}
