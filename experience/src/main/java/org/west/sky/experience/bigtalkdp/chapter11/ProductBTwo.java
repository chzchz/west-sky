package org.west.sky.experience.bigtalkdp.chapter11;

/**
 * @author chenghz
 * @date 2022/12/27 17:49
 * @description: 具体产品B2
 */
public class ProductBTwo implements IProductB{

    public ProductBTwo() {
        System.out.println("具体产品B2被创建....");
    }
}
