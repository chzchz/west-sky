package org.west.sky.experience.bigtalkdp.chapter11;

/**
 * @author chenghz
 * @date 2022/12/27 17:49
 * @description: 具体产品B1
 */
public class ProductBOne implements IProductB{

    public ProductBOne() {
        System.out.println("具体产品B1被创建....");
    }
}
