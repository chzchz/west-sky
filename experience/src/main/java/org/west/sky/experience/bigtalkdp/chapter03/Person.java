package org.west.sky.experience.bigtalkdp.chapter03;

/**
 * @author chenghz
 * @date 2022/11/21 19:56
 * @description: 被装饰的类
 */
public class Person {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    void show() {
        System.out.println(String.format("装扮的%s", name));
    }
}
