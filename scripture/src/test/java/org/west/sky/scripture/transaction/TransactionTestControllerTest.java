package org.west.sky.scripture.transaction;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * @author chenghz
 * @date 2022/8/26 14:49
 * @description:
 */
@SpringBootTest
@AutoConfigureMockMvc
class TransactionTestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testTransaction() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/transaction/test"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}