-- 滑动窗口限流，采用zset实现
-- KEYS[1]:限流key
-- ARGV[1]:时间戳，时间窗口
-- ARGV[2]:当前时间戳(作为score)
-- ARGV[3]:阈值
-- ARGV[4]:score对应的唯一value
-- 1、清除之前时间窗口的数据，只保留最近1s内的数据
redis.call("zremrangeByScore", KEYS[1], 0, ARGV[1])
-- 2、统计当前元素数量
local count = redis.call("zcard", KEYS[1])
-- 3、是否超过阈值
if (count == nil) or (count < tonumber(ARGV[3])) then
    redis.call("zadd", KEYS[1], ARGV[2], ARGV[4])
    return 1
else
    return 0
end