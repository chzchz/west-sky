-- 固定窗口限流，采用incr方式实现
-- KEYS[1]:限流key
-- ARGV[1]:阈值QPS
-- ARGV[2]:过期时间
local count = redis.call("incr", KEYS[1])
if count == 1 then
    redis.call("expire", KEYS[1], ARGV[2])
end
if count > tonumber(ARGV[1]) then
    return 0
end
return 1