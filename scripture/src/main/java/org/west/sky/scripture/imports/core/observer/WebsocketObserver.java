package org.west.sky.scripture.imports.core.observer;

import cn.hutool.json.JSONUtil;
import org.springframework.stereotype.Component;
import org.west.sky.scripture.imports.core.base.ImportDataExecuteResult;
import org.west.sky.scripture.imports.core.websocket.WebSocketManager;

import java.util.Map;

/**
 * @author: chz
 * @date: 2023/4/23
 * @description:
 */
@Component
public class WebsocketObserver implements ImportObserver {

    @Override
//    @Async
    public void update(String taskId, Map<String, Object> taskInfo, ImportDataExecuteResult result) {
        int i = 0;
        while (!WebSocketManager.connected(WebSocketManager.BUSINESS_TASK_CENTER, WebSocketManager.TASK_CENTER_PROCESS, taskId) && i++ < 50) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        //通知导入进度
        WebSocketManager.sendMsg(WebSocketManager.BUSINESS_TASK_CENTER, WebSocketManager.TASK_CENTER_PROCESS, taskId, JSONUtil.toJsonStr(result));
    }
}
