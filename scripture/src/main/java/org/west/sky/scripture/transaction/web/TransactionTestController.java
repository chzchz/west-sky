package org.west.sky.scripture.transaction.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.west.sky.scripture.transaction.service.impl.TransactionServiceA;

/**
 * @author chenghz
 * @date 2022/8/26 13:55
 * @description:
 */
@RestController
@RequestMapping("/transaction")
public class TransactionTestController {

    @Autowired
    private TransactionServiceA transactionServiceA;

    @RequestMapping("/test")
    public String testTransaction() {
        transactionServiceA.methodA();
        return "SUCCESS";
    }
}
