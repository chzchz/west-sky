package org.west.sky.scripture.strategy.springboot.one;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author chenghz
 * @date 2022/6/17 11:32
 * @description:
 */
@SpringBootTest
class CalculatorServiceTest {

    @Autowired
    private CalculatorService calculatorService;


    @Test
    public void test() {
        int result = calculatorService.calculate("multi", 4, 2);
        System.out.println("result:" + result);
    }
}