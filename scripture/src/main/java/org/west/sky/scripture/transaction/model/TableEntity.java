package org.west.sky.scripture.transaction.model;

import lombok.Data;

/**
 * @author chenghz
 * @date 2022/8/26 13:59
 * @description:
 */
@Data
public class TableEntity {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;
}
