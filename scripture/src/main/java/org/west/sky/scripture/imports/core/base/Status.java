package org.west.sky.scripture.imports.core.base;

public enum Status {

    /***
     * 请求处理成功
     */
    OK(0, "操作成功"),

    /***
     * 系统异常
     */
    FAIL_EXCEPTION(502, "系统异常");

    //自定义异常以 1000 开头

    private final int code;
    private final String message;

    Status(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int code() {
        return this.code;
    }

    public String message() {
        return this.message;
    }

    /**
     * 根据消息值返回编码
     *
     * @param value
     * @return
     */
    public static int getCode(String value) {
        for (Status eu : Status.values()) {
            if (eu.name().equals(value)) {
                return eu.code();
            }
        }
        return 0;
    }

    /**
     * 根据编码返回提示消息
     *
     * @param code
     * @return
     */
    public static String getMessage(int code) {
        for (Status eu : Status.values()) {
            if (eu.code() == code) {
                return eu.message();
            }
        }
        return null;
    }
}
