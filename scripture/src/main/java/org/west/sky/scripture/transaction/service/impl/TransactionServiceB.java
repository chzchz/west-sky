package org.west.sky.scripture.transaction.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.west.sky.scripture.transaction.model.TableEntity;
import org.west.sky.scripture.transaction.service.ITableService;

/**
 * @author chenghz
 * @date 2022/8/26 14:04
 * @description:
 */
@Service
public class TransactionServiceB {

    @Autowired
    private ITableService tableService;

    public void methodB_1() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
        throw new RuntimeException();
    }

    public void methodB_2() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
    }

    @Transactional
    public void methodB_3() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
        throw new RuntimeException();
    }

    @Transactional
    public void methodB_4() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
    }

    /**
     * REQUIRED
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void methodB_5() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
        throw new RuntimeException();
    }

    /**
     * REQUIRED
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void methodB_6() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
    }

    /**
     * REQUIRES_NEW
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void methodB_7() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
        throw new RuntimeException();
    }

    /**
     * REQUIRES_NEW
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void methodB_8() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
    }

    /**
     * SUPPORTS
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    public void methodB_9() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
        throw new RuntimeException();
    }

    /**
     * SUPPORTS
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    public void methodB_10() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
    }

    /**
     * NOT_SUPPORTED
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void methodB_11() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
        throw new RuntimeException();
    }

    /**
     * NOT_SUPPORTED
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void methodB_12() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
    }

    /**
     * MANDATORY
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public void methodB_13() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
        throw new RuntimeException();
    }

    /**
     * MANDATORY
     */
    @Transactional(propagation = Propagation.MANDATORY)
    public void methodB_14() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
    }

    /**
     * NEVER
     */
    @Transactional(propagation = Propagation.NEVER)
    public void methodB_15() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
        throw new RuntimeException();
    }

    /**
     * NEVER
     */
    @Transactional(propagation = Propagation.NEVER)
    public void methodB_16() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
    }

    /**
     * NESTED
     */
    @Transactional(propagation = Propagation.NESTED)
    public void methodB_17() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
        throw new RuntimeException();
    }

    /**
     * NESTED
     */
    @Transactional(propagation = Propagation.NESTED)
    public void methodB_18() {
        System.out.println("methodB");
        tableService.insertTableB(new TableEntity());
    }
}
