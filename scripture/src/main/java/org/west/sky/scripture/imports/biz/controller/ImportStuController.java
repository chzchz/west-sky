package org.west.sky.scripture.imports.biz.controller;

import org.springframework.context.annotation.DependsOn;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.west.sky.scripture.imports.biz.service.ImportStuService;
import org.west.sky.scripture.imports.biz.vo.StuVO;
import org.west.sky.scripture.imports.core.base.ImportController;
import org.west.sky.scripture.imports.core.base.ImportService;
import org.west.sky.scripture.imports.core.lock.LockUtil;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * author: chz
 * date: 2025/1/9
 * description:
 */
@DependsOn({"stringRedisTemplate", "springBeanUtil"})
@RestController
@RequestMapping("stu")
public class ImportStuController implements ImportController<StuVO, Object> {

    @Resource
    private ImportStuService importStuService;

    @Override
    public ImportService<StuVO, Object> importService() {
        return importStuService;
    }

    /**
     * 兼容异常导入中断，锁不能释放的情况
     */
    @PostConstruct
    public void init() {
        LockUtil.releaseAllLock();
    }
}
