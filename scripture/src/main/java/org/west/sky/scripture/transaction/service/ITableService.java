package org.west.sky.scripture.transaction.service;

import org.west.sky.scripture.transaction.model.TableEntity;

/**
 * @author chenghz
 * @date 2022/8/26 13:56
 * @description:
 */
public interface ITableService {

    void insertTableA(TableEntity tableEntity);
    void insertTableB(TableEntity tableEntity);
}
