package org.west.sky.scripture.imports.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.west.sky.scripture.imports.biz.entity.Stu;

/**
 * author: chz
 * date: 2025/1/9
 * description:
 */
public interface StuMapper extends BaseMapper<Stu> {
}
