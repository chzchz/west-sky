package org.west.sky.scripture.test;

import lombok.Getter;
import lombok.Setter;

/**
 * @author: chz
 * @date: 2023/7/28
 * @description: POS响应参数
 */
@Getter
@Setter
public class PosResponseDTO {
    /**
     * 返回码
     */
    private String code;
    /**
     * 银行行号
     */
    private String bankCode;
    /**
     * 卡号
     */
    private String cardNum;
    /**
     * 凭证号
     */
    private String trace;
    /**
     * 顾客实际扣款金额
     */
    private String realAmt;
    /**
     * 说明
     */
    private String instruction;
    /**
     * 商户号
     */
    private String merchantCode;
    /**
     * 终端号
     */
    private String terminalCode;
    /**
     * 批次号
     */
    private String batchNum;
    /**
     * 交易日期
     */
    private String tradeDate;
    /**
     * 交易时间
     */
    private String tradeHours;
    /**
     * 交易参考号
     */
    private String sysNbr;
    /**
     * 授权号
     */
    private String authorizationNum;
    /**
     * 清算日期
     */
    private String settlementDate;
    /**
     * lrc
     */
    private String lrc;
    /**
     * 原交易金额
     */
    private String refAmt;
    /**
     * 优惠金额
     */
    private String discountAmt;
    /**
     * 交易类型
     */
    private String transType;
    /**
     * 联盟流水号
     */
    private String unionSerialNum;
    /**
     * 活动内容
     */
    private String activityContent;
    /**
     * 优惠信息
     */
    private String discountMsg;
    /**
     * 收银机号
     */
    private String posId;
    /**
     * 收银员号
     */
    private String operId;
    /**
     * 商户订单号
     */
    private String merchantOrderId;
    /**
     * 借贷记标识
     */
    private String debitSign;
    /**
     * 预留字段
     */
    private String remark;
    /**
     * 折扣金额
     */
    private String deductionAmt;
    /**
     * 优惠券抵扣金额
     */
    private String couponDeductionAmt;
    /**
     * 活动信息
     */
    private String activityMsg;
    /**
     * 分公司信息
     */
    private String branchOffice;
}
