package org.west.sky.scripture.imports.core.base;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author: chz
 * @date: 2023/4/13
 * @description: excel数据错误原因
 */
@Data
public class ImportData {

    /**
     * 是否为正确数据：数据格式正确、业务合法
     */
    @ExcelIgnore
    private boolean correct = true;
    /**
     * 错误信息
     */
    @ExcelProperty("错误原因")
    private String errorMsg;


    public void addErrorMsg(String msg) {
        if (this.errorMsg == null) {
            this.errorMsg = msg;
        } else {
            this.errorMsg = this.errorMsg + ";" + msg;
        }
    }
}
