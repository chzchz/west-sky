package org.west.sky.scripture.imports.biz.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.west.sky.scripture.imports.core.base.ImportData;

import javax.validation.constraints.NotBlank;

/**
 * author: chz
 * date: 2025/1/9
 * description:
 */
@Getter
@Setter
public class StuVO extends ImportData {
    /**
     * 编号
     */
    @Length(min = 1, max = 10, message = "编号长度不能超过10个字符")
    @ExcelProperty("编号")
    private String stuNo;
    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    @ExcelProperty("姓名")
    private String stuName;
}
