package org.west.sky.scripture.elegantdemo.service;

import org.springframework.stereotype.Service;
import org.west.sky.scripture.elegantdemo.annotation.DictConvert;
import org.west.sky.scripture.elegantdemo.request.TestVO;
import org.west.sky.scripture.elegantdemo.response.dto.TestDTO;
import org.west.sky.scripture.test.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author chenghz
 * @date 2022/6/21 10:01
 * @description:
 */
@Service
public class ITestServiceImpl implements ITestService {

    @Override
    public String save(TestVO testVO) {
        return "保存成功";
    }

    @Override
    public TestVO get(Integer id) {
        TestVO vo = new TestVO();
        vo.setName("test");
        vo.setPrice(BigDecimal.valueOf(1));
        vo.setNum(10);
        return vo;
    }

    @Override
    @DictConvert
    public TestDTO get4() {
        TestDTO dto = new TestDTO();
        dto.setName("zhangsan");
        dto.setSex("1");
        return dto;
    }

    @Override
    @DictConvert
    public List<TestDTO> get5() {
        TestDTO dto1 = new TestDTO();
        dto1.setName("zhangsan");
        dto1.setSex("1");

        TestDTO dto2 = new TestDTO();
        dto2.setName("lisi");
        dto2.setSex("2");
        List<TestDTO> list = new ArrayList<>();
        list.add(dto1);
        list.add(dto2);
        return list;
    }
}
