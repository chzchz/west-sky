package org.west.sky.scripture.imports.taskCenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.west.sky.scripture.imports.taskCenter.entity.TaskCenterRecord;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 任务中心(TaskCenter)表数据库访问层
 *
 * @author chz
 * @since 2023-04-14 14:06:16
 */
public interface TaskCenterRecordMapper extends BaseMapper<TaskCenterRecord> {

    /**
     * 查询最近一周的任务记录
     *
     * @param userId
     * @return
     */
    List<TaskCenterRecord> listLastWeekRecord(@Param("userId") Integer userId, @Param("weekAgo") LocalDateTime weekAgo, @Param("now") LocalDateTime now);

}

