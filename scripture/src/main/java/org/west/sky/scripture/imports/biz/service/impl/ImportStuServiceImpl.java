package org.west.sky.scripture.imports.biz.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import org.west.sky.scripture.imports.biz.entity.Stu;
import org.west.sky.scripture.imports.biz.mapper.StuMapper;
import org.west.sky.scripture.imports.biz.service.ImportStuService;
import org.west.sky.scripture.imports.biz.vo.StuVO;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * author: chz
 * date: 2025/1/9
 * description:
 */
@Service
public class ImportStuServiceImpl implements ImportStuService {

    @Resource
    private StuMapper stuMapper;

    @Override
    public void checkData(List<StuVO> data, Object params) {
        List<Stu> stus = stuMapper.selectList(new QueryWrapper<>());
        List<String> stuNos = stus.stream().map(Stu::getStuNo).collect(Collectors.toList());
        for (StuVO vo : data) {
            //模拟业务校验
            if (stuNos.contains(vo.getStuNo())) {
                vo.setCorrect(false);
                vo.setErrorMsg("编号已存在");
            }
        }
    }

    @Override
    public void saveData(List<StuVO> data, Object params) {
        List<Stu> stus = BeanUtil.copyToList(data, Stu.class);
        stuMapper.insert(stus);
        try {
            Thread.sleep(10000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Map<String, Object> getTaskInfo() {
        Map<String, Object> map = new HashMap<>();
        map.put("errorFileName", "学员信息导入");
        return map;
    }
}
