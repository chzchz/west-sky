package org.west.sky.scripture.elegantdemo.request;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author chenghz
 * @date 2022/6/21 10:15
 * @description:
 */
@Data
public class TestVO {

    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    @Length(min = 1, max = 5, message = "名称不能少于1个字，对于5个字")
    private String name;
    /**
     * 价格
     */
    @NotNull(message = "价格不能为空")
    @Min(value = 1, message = "价格不能低于1元")
    private BigDecimal price;
    /**
     * 数量
     */
    @NotNull(message = "数量不能为空")
    @Min(value = 1, message = "数量不能低于1个")
    private Integer num;
}
