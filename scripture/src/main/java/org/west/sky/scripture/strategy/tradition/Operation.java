package org.west.sky.scripture.strategy.tradition;

/**
 * @author chenghz
 * @date 2022/6/17 9:14
 * @description: 策略接口
 */
public interface Operation {

    /**
     * 执行计算
     *
     * @param a
     * @param b
     * @return
     */
    int calculate(int a, int b);
}
