package org.west.sky.scripture.transaction.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.west.sky.scripture.transaction.mapper.ITableMapper;
import org.west.sky.scripture.transaction.model.TableEntity;
import org.west.sky.scripture.transaction.service.ITableService;

/**
 * @author chenghz
 * @date 2022/8/26 14:05
 * @description:
 */
@Service
public class TableServiceImpl implements ITableService {

    @Autowired
    ITableMapper tableMapper;

    @Override
    public void insertTableA(TableEntity tableEntity) {
        tableMapper.insertTableA(tableEntity);
    }

    @Override
    public void insertTableB(TableEntity tableEntity) {
        tableMapper.insertTableB(tableEntity);
    }
}
