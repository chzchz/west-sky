package org.west.sky.scripture.test;

import org.apache.commons.collections4.list.TreeList;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.*;

/**
 * @Author: chz
 * @Description:
 * @Date: 2022/3/3
 */
public class Test {


    /**
     * 自定义线程名称,方便的出错的时候溯源
     */

    public static void main(String[] args) {
//        List<String> list = new ArrayList<>();
//        List<String> vector = new Vector<>();
//        List<String> linkedList = new LinkedList<>();
//        new HashSet<>();
//        new TreeSet<>();
//        new LinkedHashSet<>();
//
//        new Thread();
//        Executors.newCachedThreadPool();
//        new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue<Runnable>());
//
//        Executors.newFixedThreadPool(4);
//        new ThreadPoolExecutor(4,4,0,TimeUnit.MILLISECONDS,new LinkedBlockingDeque<Runnable>());
//
//        Executors.newSingleThreadExecutor();
//        new ThreadPoolExecutor(1,1,0,TimeUnit.MILLISECONDS,new LinkedBlockingDeque<Runnable>());
//
//        Executors.newScheduledThreadPool(1);
//        new ScheduledThreadPoolExecutor(1);
//
//        System.out.println(Runtime.getRuntime().availableProcessors());
//        getResponseDto("00招商银行621483******6803    000155000000000001交易成功交易成功交易成功交易成功交易成0001");
        TreeSet<Integer> list = new TreeSet<>();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            list.add(random.nextInt(81));
        }
        System.out.println(list);
    }

        /**
         * 包装响应报文
         */
        private static PosResponseDTO getResponseDto(String res) {
            PosResponseDTO responseDto = new PosResponseDTO();
            responseDto.setCode(bSubstring(res, 2));
            System.out.println(bSubstring(res, 2));
            res = res.substring(responseDto.getCode().length());
            responseDto.setBankCode(bSubstring(res, 8));
            System.out.println(bSubstring(res, 8));

            res = res.substring(responseDto.getBankCode().length());
            responseDto.setCardNum(bSubstring(res, 20));
            System.out.println(bSubstring(res, 20));
            res = res.substring(responseDto.getCardNum().length());
            responseDto.setTrace(bSubstring(res, 6));
            System.out.println(bSubstring(res, 6));
            res = res.substring(responseDto.getTrace().length());
            responseDto.setRealAmt(bSubstring(res, 12));
            System.out.println(bSubstring(res, 12));
            res = res.substring(responseDto.getRealAmt().length());
            responseDto.setInstruction(bSubstring(res, 40));
            System.out.println(bSubstring(res, 40));
//            res = res.substring(responseDto.getInstruction().length());
//            responseDto.setMerchantCode(bSubstring(res, 15));
//            res = res.substring(responseDto.getMerchantCode().length());
//            responseDto.setTerminalCode(bSubstring(res, 8));
//            res = res.substring(responseDto.getTerminalCode().length());
//            responseDto.setBatchNum(bSubstring(res, 6));
//            res = res.substring(responseDto.getBatchNum().length());
//            responseDto.setTradeDate(bSubstring(res, 4));
//            res = res.substring(responseDto.getTradeDate().length());
//            responseDto.setTradeHours(bSubstring(res, 6));
//            res = res.substring(responseDto.getTradeHours().length());
//            responseDto.setSysNbr(bSubstring(res, 15));
//            res = res.substring(responseDto.getSysNbr().length());
//            responseDto.setAuthorizationNum(bSubstring(res, 6));
//            res = res.substring(responseDto.getAuthorizationNum().length());
//            responseDto.setSettlementDate(bSubstring(res, 4));
//            res = res.substring(responseDto.getSettlementDate().length());
//            responseDto.setLrc(bSubstring(res, 18));
//            res = res.substring(responseDto.getLrc().length());
//            responseDto.setRefAmt(bSubstring(res, 12));
//            res = res.substring(responseDto.getRefAmt().length());
//            responseDto.setDiscountAmt(bSubstring(res, 12));
//            res = res.substring(responseDto.getDiscountAmt().length());
//            responseDto.setTransType(bSubstring(res, 2));
//            res = res.substring(responseDto.getTransType().length());
//            responseDto.setUnionSerialNum(bSubstring(res, 30));
//            res = res.substring(responseDto.getUnionSerialNum().length());
//            responseDto.setActivityContent(bSubstring(res, 60));
//            res = res.substring(responseDto.getActivityContent().length());
//            responseDto.setDiscountMsg(bSubstring(res, 60));
//            res = res.substring(responseDto.getDiscountMsg().length());
//            responseDto.setPosId(bSubstring(res, 8));
//            res = res.substring(responseDto.getPosId().length());
//            responseDto.setOperId(bSubstring(res, 8));
//            res = res.substring(responseDto.getOperId().length());
//            responseDto.setMerchantOrderId(bSubstring(res, 30));//358 388
//            res = res.substring(responseDto.getMerchantOrderId().length() + 60);
//            responseDto.setDebitSign(bSubstring(res, 60));//448,508
//            res = res.substring(responseDto.getDebitSign().length());
//            responseDto.setRemark(bSubstring(res, 60));
//            res = res.substring(responseDto.getRemark().length());
//            responseDto.setDeductionAmt(bSubstring(res, 12));
//            res = res.substring(responseDto.getDeductionAmt().length());
//            responseDto.setCouponDeductionAmt(bSubstring(res, 12));
//            res = res.substring(responseDto.getCouponDeductionAmt().length());
//            responseDto.setActivityMsg(bSubstring(res, 200));
//            res = res.substring(responseDto.getActivityMsg().length());
//            responseDto.setBranchOffice(bSubstring(res, 50));//793,842
            return responseDto;
        }




    /**
     * 按字节起始位置截取
     *
     * @return
     */
    private static String bSubstring(String original, int count) {
//        if (org.apache.commons.lang3.StringUtils.isNotBlank(original)) {
            try {
                if (count > 0 && count < original.getBytes("GBK").length) {
                    StringBuilder buff = new StringBuilder();
                    char c;
                    for (int i = 0; i < count; i++) {
                        c = original.charAt(i);
                        buff.append(c);
                        if (isChineseChar(c)) {
                            --count;
                        }
                    }
                    return buff.toString();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return original;
//        }
//        return original;
    }

    /**
     * 判断是否为汉字
     *
     * @param c
     * @return
     */
    private static boolean isChineseChar(char c) throws UnsupportedEncodingException {
        return String.valueOf(c).getBytes("GBK").length > 1;
    }
}
