package org.west.sky.scripture;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

/**
 * @author chz
 * @date 2022/3/21
 * @description: 启动类
 */
@EnableWebSocket
@SpringBootApplication
@MapperScan(basePackages = "org.west.sky.scripture.**.mapper")
public class ScriptureApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScriptureApplication.class, args);
    }
}
