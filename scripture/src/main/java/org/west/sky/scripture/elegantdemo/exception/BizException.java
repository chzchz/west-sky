package org.west.sky.scripture.elegantdemo.exception;

import lombok.Getter;
import org.west.sky.scripture.elegantdemo.response.BizCode;
import org.west.sky.scripture.elegantdemo.response.StatusCode;

/**
 * @author chenghz
 * @date 2022/6/21 10:53
 * @description: 业务异常
 */
@Getter
public class BizException extends RuntimeException {

    private final int code;

    private final String msg;

    /**
     * 手动设置异常
     *
     * @param statusCode
     * @param message
     */
    public BizException(StatusCode statusCode, String message) {
        // message用于用户设置抛出错误详情，例如：当前价格-5，小于0
        super(message);
        // 状态码
        this.code = statusCode.getCode();
        // 状态码配套的msg
        this.msg = statusCode.getMsg();
    }

    /**
     * 默认异常使用APP_ERROR状态码
     *
     * @param message
     */
    public BizException(String message) {
        super(message);
        this.code = BizCode.BIZ_ERROR.getCode();
        this.msg = BizCode.BIZ_ERROR.getMsg();
    }
}
