package org.west.sky.scripture.strategy.tradition;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author chenghz
 * @date 2022/6/17 9:18
 * @description:
 */
public class OperatorFactory {

    private static Map<String, Operation> operationMap = new HashMap<>(4);

    static {
        operationMap.put("add", new AddOperation());
        operationMap.put("sub", new SubOperation());
        operationMap.put("multi", new MultiOperation());
        operationMap.put("div", new DivOperation());
    }

    /**
     * 获取对应目标实现类
     *
     * @param operator
     * @return
     */
    public static Optional<Operation> getOperation(String operator) {
        return Optional.ofNullable(operationMap.get(operator));
    }
}
