package org.west.sky.scripture.imports.taskCenter.vo;//package com.runa.smarter.equipment.web.modules.taskCenter.vo;
//
//import com.runa.smarter.charge.web.modules.approve.vo.WarnNumVO;
//import com.runa.smarter.charge.web.modules.taskCenter.entity.TaskCenterRecord;
//import lombok.Data;
//
//import java.util.List;
//
///**
// * @author: chz
// * @date: 2023/4/26
// * @description:
// */
//@Data
//public class TaskVO {
//
//    /**
//     * 待办任务
//     */
//    private List<WarnNumVO> auditTask;
//    /**
//     * 导入任务
//     */
//    private List<TaskCenterRecord> importTask;
//    /**
//     * 批量执行任务
//     */
//    private List<TaskCenterRecord> batchExecuteTask;
//}
