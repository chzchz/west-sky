package org.west.sky.scripture.strategy.tradition;

/**
 * @author chenghz
 * @date 2022/6/17 9:22
 * @description:
 */
public class Test {

    public static void main(String[] args) {
        Operation operation = OperatorFactory.getOperation("div")
                .orElseThrow(() -> new IllegalArgumentException("Invalid Operator"));
        int calculate = operation.calculate(6, 3);
        System.out.println(calculate);
    }
}
