package org.west.sky.scripture.elegantdemo.annotation;

import java.lang.annotation.*;

/**
 * 数据字典转换
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Dict {

    /**
     * 是否是本地字典
     * 本地字典：使用dictVal值做解析
     * 非本地字典：集合字典服务查询回来
     **/
    boolean isLocal() default false;

    /**
     * 字典存放后缀
     * 默认 "Value"
     * 例 原始字段名："sex"  翻译储存的字段名："sexValue"
     **/
    String suffix() default "Value";

    /**
     * 本地字典键值对，当isLocal=true时有效
     * 例如：1：草稿；2：已提交；3：已删除
     * 为了方便，使用的是中文的：和；
     **/
    String dictVal() default "";

    /**
     * 在线字典编码，当isLocal=false时有效
     * 例如：dictCode=SEX，则会调用 DictDataFeignClient.selectDictData("SEX");去获取字典值
     */
    String dictCode() default "";

}
