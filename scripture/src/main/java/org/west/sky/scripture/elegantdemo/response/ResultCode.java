package org.west.sky.scripture.elegantdemo.response;

import lombok.Getter;

/**
 * @author chenghz
 * @date 2022/6/21 10:08
 * @description:
 */
@Getter
public enum ResultCode implements StatusCode {
    /**
     * 响应码和对应的描述
     */
    SUCCESS(0, "请求成功"),

    FAILED(1001, "请求失败"),

    VALIDATE_ERROR(1002, "参数校验失败"),

    RESPONSE_PACK_ERROR(1003, "response返回包装失败");

    private final int code;

    private final String msg;

    ResultCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
