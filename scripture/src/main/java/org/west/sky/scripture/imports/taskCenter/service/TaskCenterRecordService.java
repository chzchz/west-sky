package org.west.sky.scripture.imports.taskCenter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.west.sky.scripture.imports.taskCenter.entity.TaskCenterRecord;


/**
 * 任务中心(TaskCenter)表服务接口
 *
 * @author chz
 * @since 2023-04-14 14:06:20
 */
public interface TaskCenterRecordService extends IService<TaskCenterRecord> {
}

