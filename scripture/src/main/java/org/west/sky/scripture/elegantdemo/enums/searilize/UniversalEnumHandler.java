//package org.west.sky.scripture.elegantdemo.enums.searilize;
//
//import com.rn.erp.ems.core.enums.BaseEnum;
//import com.rn.erp.ems.tool.utils.EnumUtil;
//import org.apache.ibatis.type.BaseTypeHandler;
//import org.apache.ibatis.type.JdbcType;
//
//import java.sql.CallableStatement;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
///**
// * @author: chz
// * @date: 2023/3/17
// * @description:
// */
//public class UniversalEnumHandler<E extends Enum<E> & BaseEnum> extends BaseTypeHandler<E> {
//
//    private final Class<E> type;
//
//    public UniversalEnumHandler(Class<E> type) {
//        if (type == null) {
//            throw new IllegalArgumentException("Type argument cannot be null");
//        }
//        this.type = type;
//    }
//
//    @Override
//    public void setNonNullParameter(PreparedStatement preparedStatement, int i, E parameter, JdbcType jdbcType) throws SQLException {
//        preparedStatement.setInt(i, parameter.getCode());
//    }
//
//    @Override
//    public E getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
//        int code = resultSet.getInt(columnName);
//        return resultSet.wasNull() ? null : EnumUtil.ofCode(this.type, code);
//    }
//
//    @Override
//    public E getNullableResult(ResultSet resultSet, int columnIndex) throws SQLException {
//        int code = resultSet.getInt(columnIndex);
//        return resultSet.wasNull() ? null : EnumUtil.ofCode(this.type, code);
//    }
//
//    @Override
//    public E getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
//        int code = callableStatement.getInt(columnIndex);
//        return callableStatement.wasNull() ? null : EnumUtil.ofCode(this.type, code);
//    }
//}
