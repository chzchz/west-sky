//package org.west.sky.scripture.elegantdemo.enums.searilize;
//
//import com.rn.erp.ems.core.enums.BaseEnum;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Objects;
//import java.util.concurrent.ConcurrentHashMap;
//
///**
// * @author: chz
// * @date: 2023/3/17
// * @description:
// */
//public class EnumUtil {
//
//    private static final Map<Class<? extends Enum<?>>, Map<Integer, ? extends Enum<? extends BaseEnum>>> CLASS_ENUM_MAP =
//            new ConcurrentHashMap<>(16);
//
//    @SuppressWarnings("unchecked")
//    public static <E extends Enum<E> & BaseEnum> E ofCode(Class<E> enumClass, Integer code) {
//        Map<Integer, ? extends Enum<? extends BaseEnum>> enumMap = CLASS_ENUM_MAP.get(enumClass);
//        if (Objects.isNull(enumMap)) {
//            Map<Integer, E> map = new HashMap<>();
//            for (E enumConstant : enumClass.getEnumConstants()) {
//                map.put(enumConstant.getCode(), enumConstant);
//            }
//            CLASS_ENUM_MAP.putIfAbsent(enumClass, map);
//            return map.get(code);
//        }
//        return (E) enumMap.get(code);
//    }
//}
