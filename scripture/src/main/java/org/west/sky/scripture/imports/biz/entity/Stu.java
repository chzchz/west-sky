package org.west.sky.scripture.imports.biz.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * author: chz
 * date: 2025/1/9
 * description:
 */
@Data
@TableName("task_stu")
public class Stu extends Model<Stu> {
    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 编号
     */
    private String stuNo;
    /**
     * 姓名
     */
    private String stuName;
}
