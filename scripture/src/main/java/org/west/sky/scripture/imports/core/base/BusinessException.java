package org.west.sky.scripture.imports.core.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class BusinessException extends RuntimeException {

    private int code;
    private String msg;

    public BusinessException(int code) {
        this.code = code;
        this.msg = Status.getMessage(code);
    }

    public BusinessException(int code, Throwable e) {
        super(e);
        this.code = code;
        this.msg = Status.getMessage(code);
    }

    public BusinessException(String msg) {
        super(msg);
        this.code = Status.FAIL_EXCEPTION.code();
        this.msg = msg;
    }

    public BusinessException(String msg, Object... args) {
        super(msg);
        this.code = Status.FAIL_EXCEPTION.code();
        this.msg = String.format(msg, args);
    }

    public BusinessException(String msg, Throwable e) {
        super(msg, e);
        this.code = Status.FAIL_EXCEPTION.code();
        this.msg = msg;
    }

    public BusinessException(Status status) {
        super(status.message());
        this.code = status.code();
        this.msg = status.message();
    }


    @Override
    public StackTraceElement[] getStackTrace() {
        return null;
    }
}
