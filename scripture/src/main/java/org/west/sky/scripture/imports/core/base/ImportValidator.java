package org.west.sky.scripture.imports.core.base;

import com.alibaba.excel.annotation.ExcelProperty;
//import jakarta.validation.constraints.NotEmpty;
//import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: chz
 * @date: 2023/4/13
 * @description: excel 格式、表头校验
 */
@Slf4j
public class ImportValidator {

    /**
     * 是否是2003的excel，返回true是2003
     *
     * @param filePath
     * @return
     */
    public static boolean isExcel2003(String filePath) {
        return filePath.matches("^.+\\.(?i)(xls)$");
    }

    /**
     * 是否是2007的excel，返回true是2007
     *
     * @param filePath
     * @return
     */
    public static boolean isExcel2007(String filePath) {
        return filePath.matches("^.+\\.(?i)(xlsx)$");
    }

    /**
     * 验证EXCEL文件
     *
     * @param filePath
     * @return
     */
    public static boolean validateExcel(String filePath) {
        return filePath != null && (isExcel2003(filePath) || isExcel2007(filePath));
    }

    /**
     * 获取表头(一行）
     *
     * @param headMap
     * @return
     */
    private static List<String> getExcelTitle(Map<Integer, String> headMap) {
        //遍历获取第一行表头，存入keyList
        List<String> titleList = new ArrayList<>();
        Set<Integer> integerSet = headMap.keySet();
        for (int i1 = 0; i1 < integerSet.size(); i1++) {
            titleList.add(headMap.get(i1));
        }
        return titleList;
    }


    /**
     * 校验Excel表头
     *
     * @param headMap
     * @param fields
     * @return
     */
    public static boolean validatorExcelHeads(Map<Integer, String> headMap, Field[] fields) {
        List<String> titleList = getExcelTitle(headMap);
        // 遍历字段进行判断
        int num = 0;
        for (Field field : fields) {
            // 获取当前字段上的ExcelProperty注解信息
            ExcelProperty fieldAnnotation = field.getAnnotation(ExcelProperty.class);
            // 判断当前字段上是否存在ExcelProperty注解
            if (fieldAnnotation != null) {
                num++;
                // 存在ExcelProperty注解则根据注解的index索引到表头中获取对应的表头名
                Object tile = titleList.get(fieldAnnotation.index());
                if (null == tile) {
                    return false;
                }
                // 判断表头是否为空或是否和当前字段设置的表头名不相同
                if ("".equals(tile.toString()) || !tile.toString().equals(fieldAnnotation.value()[0])) {
                    // 如果为空或不相同，则抛出异常不再往下执行
                    return false;
                }
            }
        }
        return num == titleList.size();
    }

    public static boolean validatorExcelHeadsNoIndex(Map<Integer, String> headMap, Field[] fields) {
        List<String> titleList = getExcelTitle(headMap);
        // 遍历字段进行判断
        for (Field field : fields) {
            // 获取当前字段上的ExcelProperty注解信息
            ExcelProperty fieldAnnotation = field.getAnnotation(ExcelProperty.class);
            // 判断当前字段上是否存在ExcelProperty注解
            if (fieldAnnotation != null && fieldAnnotation.value() != null && fieldAnnotation.value().length > 0 && StringUtils.hasText(fieldAnnotation.value()[0])) {
                if (!titleList.contains(fieldAnnotation.value()[0])) {
                    log.info("导入列表缺少:{}", fieldAnnotation.value()[0]);
                    return false;
                }
            }
        }
        return true;
    }

}
