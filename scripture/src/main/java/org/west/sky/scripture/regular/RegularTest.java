package org.west.sky.scripture.regular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author chz
 * @date 2022/4/2
 * @description: 正则表达式测试
 */
public class RegularTest {
    public static void main(String[] args) {
        testNo4();
    }

    /**
     * 案例1
     */
    private static void testNo1() {
        //从字符串 str 中提取数字部分的内容(匹配一次)
        String str = "abc123def";
        //下面两个表达式等价。
        // "+"  一次或多次匹配前面的字符或子表达式,等效于{1,}
        //"[0-9]" 匹配数字，等效于\\d
        String patt1 = "[0-9]+";
        String patt2 = "[0-9]{1,}";

        Pattern compile = Pattern.compile(patt2);
        Matcher matcher = compile.matcher(str);
        if (matcher.find()) {
            System.out.println("Found value: " + matcher.group(0));
        } else {
            System.out.println("NO MATCH");
        }
    }

    /**
     * 案例2
     */
    private static void testNo2() {
        //匹配以0个或多个数字开头，abc结束的字符串
        String str1 = "哈哈哈abc";
        String str2 = "ww123abc";
        String str3 = "123滚滚滚4abc";
        String str4 = "1234abcd";
        String str5 = "1234abc";
        //下面的正则表达式分为三个部分
        //1、 ^[0-9]*  以数字开头，个数可以是0个或者任意个,等价于^[0-9]{0,}、^\\d{0,}
        //2、 .*  匹配除了\r\n以外的任意字符串，数量不限
        //3、 abc$  匹配以abc结尾的字符串
        //"*"  零次或多次匹配前面的字符或子表达式,等效于{0,}
        //"."  匹配除"\r\n"之外的任何单个字符
        //"$"  匹配输入字符串结尾的位置
        String patt1 = "^[0-9]*.*abc$";
        String patt2 = "^\\d{0,}.*abc$";

        System.out.println(Pattern.matches(patt2, str1));
        System.out.println(Pattern.matches(patt2, str2));
        System.out.println(Pattern.matches(patt2, str3));
        System.out.println(Pattern.matches(patt2, str4));
        System.out.println(Pattern.matches(patt2, str5));
    }

    private static void testNo3(){
        //"数字（年|财年）能源消耗总量（为|是）"


        String pattern = "^[0-9]*(年|财年)?(能源消耗总量|能源消耗量)([为是])?$";

        System.out.println(Pattern.matches(pattern, "2021财年源消耗量为"));
    }

    private static void testNo4(){
        //"2021年汽油使用量"
        //"2019年汽油使用量"
        //"2020年汽油使用量"
        //"2021年汽油能源消耗量"
        //"2020年汽油消耗量为"
        //"2021财年汽油消耗总量"
        //"2021年汽油能耗量"烧碱（含自用量）



//        String pattern = "^[0-9]*(年|财年)?(汽油使用量|汽油能源消耗量|汽油消耗量|汽油消耗总量|汽油能耗量)(约为|为|是|约是)?$";
        String pattern = "^((?!吨钢|单位|、|，).)*(氨氮排放浓度|氨氮平均排放浓度|氨氮年平均排放浓度|NH3-N排放浓度)$";

        System.out.println(Pattern.matches(pattern, "惠康生物氨氮排放浓度"));
    }
}
