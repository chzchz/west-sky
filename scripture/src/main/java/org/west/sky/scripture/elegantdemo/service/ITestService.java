package org.west.sky.scripture.elegantdemo.service;

import org.west.sky.scripture.elegantdemo.request.TestVO;
import org.west.sky.scripture.elegantdemo.response.dto.TestDTO;

import java.util.List;

/**
 * @author chenghz
 * @date 2022/6/21 10:01
 * @description:
 */
public interface ITestService {

    /**
     * 保存
     *
     * @param testVO
     * @return
     */
    String save(TestVO testVO);

    /**
     * 获取
     *
     * @param id
     * @return
     */
    TestVO get(Integer id);

    /**
     * 字典转换
     *
     * @return
     */
    TestDTO get4();

    /**
     * 字典转换
     *
     * @return
     */
    List<TestDTO> get5();
}
