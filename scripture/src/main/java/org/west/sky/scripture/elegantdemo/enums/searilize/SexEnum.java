//package org.west.sky.scripture.elegantdemo.enums.searilize;
//
//import com.fasterxml.jackson.annotation.JsonCreator;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import com.rn.erp.ems.tool.utils.EnumUtil;
//import lombok.AllArgsConstructor;
//
//@AllArgsConstructor
//@JsonFormat(shape = JsonFormat.Shape.OBJECT)
//public enum SexEnum implements BaseEnum {
//    BOY(1, "男"),
//    GIRL(2, "女"),
//    GHOST(3, "未知");
//
//    private final Integer code;
//
//    private final String value;
//
//    @Override
//    public Integer getCode() {
//        return this.code;
//    }
//
//    @Override
//    public String getValue() {
//        return this.value;
//    }
//
//
//    @JsonCreator
//    public static SexEnum forValue(int code)
//    {
//        return EnumUtil.ofCode(SexEnum.class, code);
//    }
//}
