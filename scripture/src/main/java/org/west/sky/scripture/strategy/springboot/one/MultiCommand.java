package org.west.sky.scripture.strategy.springboot.one;

import org.springframework.stereotype.Component;

/**
 * @author chenghz
 * @date 2022/6/17 11:18
 * @description:
 */
@Component
public class MultiCommand implements Command {
    @Override
    public String operatorType() {
        return "multi";
    }

    @Override
    public Integer execute(int a, int b) {
        return a * b;
    }
}
