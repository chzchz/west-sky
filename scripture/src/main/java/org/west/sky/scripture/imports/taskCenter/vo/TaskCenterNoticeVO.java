package org.west.sky.scripture.imports.taskCenter.vo;

import lombok.Data;

/**
 * @author: chz
 * @date: 2023/4/26
 * @description: 任务中心通知推送
 */
@Data
public class TaskCenterNoticeVO {

    /**
     * 通知类型 1:弹窗暂时推送信息 2:刷新待办任务数量
     */
    private Integer noticeType;
    /**
     * 任务类型
     */
    private String taskType;
    /**
     * 消息内容
     */
    private String noticeMsg;
}
