package org.west.sky.scripture.elegantdemo.enums.searilize;//package com.rn.erp.ems.core.config;
//
//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.*;
//import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
//import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
//import com.rn.erp.ems.core.enums.BaseEnum;
//import com.rn.erp.ems.tool.utils.EnumUtil;
//
//import java.io.IOException;
//
///**
// * @author: chz
// * @date: 2023/3/17
// * @description:
// */
//public class TypeEnumDeserializer extends StdDeserializer<BaseEnum> implements ContextualDeserializer {
//
//    public TypeEnumDeserializer() {
//        super((JavaType) null);
//    }
//    public TypeEnumDeserializer(JavaType valueType) {
//        super(valueType);
//    }
//
//    @Override
//    public JsonDeserializer<?> createContextual(DeserializationContext deserializationContext, BeanProperty beanProperty){
//        return new TypeEnumDeserializer(beanProperty.getType());
//    }
//
//    @Override
//    @SuppressWarnings("all")
//    public BaseEnum deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
//        return (BaseEnum) EnumUtil.ofCode((Class) _valueClass, jsonParser.getIntValue());
//    }
//
//
//}
