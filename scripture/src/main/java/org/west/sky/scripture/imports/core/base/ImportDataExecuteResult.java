package org.west.sky.scripture.imports.core.base;

import lombok.Data;

/**
 * @author: chz
 * @date: 2023/4/14
 * @description: excel数据执行结果
 */
@Data
public class ImportDataExecuteResult {
    /**
     * 执行状态(0:进行中，1:结束)
     */
    int executeStatus;
    /**
     * 当前执行进度
     */
    Progress progress;
    /**
     * 执行总数量
     */
    private Integer totalNum;
    /**
     * 累计成功数量
     */
    private Integer successNum;
    /**
     * 累计失败数量
     */
    private Integer failNum;
    /**
     * 文件地址
     */
    private String fileUrl;

    public ImportDataExecuteResult(int executeStatus, Integer totalNum, Integer successNum, Integer failNum, String fileUrl) {
        this(executeStatus, null, totalNum, successNum, failNum, fileUrl);
    }

    public ImportDataExecuteResult(int executeStatus, Progress progress, Integer totalNum, Integer successNum, Integer failNum, String fileUrl) {
        this.executeStatus = executeStatus;
        this.progress = progress;
        this.totalNum = totalNum;
        this.successNum = successNum;
        this.failNum = failNum;
        this.fileUrl = fileUrl;
    }

    public enum Progress {
        /**
         * 文件校验结束
         */
        FILE_VERIFY_ENDED,
        /**
         * 解析完成
         */
        ANALYSIS_ENDED,
        /**
         * 导入中
         */
        IMPORTING,
        /**
         * 导入结束
         */
        IMPORT_ENDED;
    }
}
