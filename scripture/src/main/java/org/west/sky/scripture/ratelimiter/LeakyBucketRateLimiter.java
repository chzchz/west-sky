package org.west.sky.scripture.ratelimiter;

import java.time.LocalTime;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author chz
 * @date 2022/3/18
 * @description: 漏桶限流算法
 */
public class LeakyBucketRateLimiter {
    /**
     * 流水速率
     */
    private Integer rate;
    /**
     * 桶的大小
     */
    private Integer burst;
    /**
     * 有界队列，相当于桶，用来存储数据
     */
    private ArrayBlockingQueue<String> queue;

    public LeakyBucketRateLimiter(int rate, int burst) {
        this.rate = rate;
        this.burst = burst;
        this.queue = new ArrayBlockingQueue<>(burst);
        //创建一个定时执行的线程池,延迟1s后，指定毫秒执行一次
        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                String take = queue.poll();
                if (null == take) {
                    System.exit(0);
                }
                System.out.println(LocalTime.now() + " " + take + "被执行了");
            }
        }, 500, 1000 / rate, TimeUnit.MILLISECONDS);
    }

    /**
     * 往桶里面丢数据
     *
     * @param data
     */
    public synchronized boolean tryAcquire(String data) {
        try {
            return queue.add(data);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        LeakyBucketRateLimiter bucketRateLimiter = new LeakyBucketRateLimiter(1, 10);
        for (int i = 0; i < 20; i++) {
            Thread.sleep(200);
            if (bucketRateLimiter.tryAcquire(String.format("第%d个请求", i + 1))) {
                System.out.println(LocalTime.now() + " " + (i + 1) + " 干点什么");
            } else {
                System.out.println(LocalTime.now() + " " + (i + 1) + " 被限流了");
            }
        }
    }

}
