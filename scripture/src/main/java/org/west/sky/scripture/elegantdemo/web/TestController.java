package org.west.sky.scripture.elegantdemo.web;

import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.west.sky.scripture.elegantdemo.annotation.NotControllerResponseAdvice;
import org.west.sky.scripture.elegantdemo.request.TestVO;
import org.west.sky.scripture.elegantdemo.response.ResultVO;
import org.west.sky.scripture.elegantdemo.response.dto.TestDTO;
import org.west.sky.scripture.elegantdemo.service.ITestService;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author chenghz
 * @date 2022/6/21 9:59
 * @description:
 */
@Validated
@RestController
@AllArgsConstructor
@RequestMapping("test")
public class TestController {

    private final ITestService testService;

    /**
     * 实体参数校验、带ResultVO返回
     *
     * @param testVO
     * @return
     */
    @PostMapping("save")
    public ResultVO save(@Validated @RequestBody TestVO testVO) {
        return new ResultVO(testService.save(testVO));
    }

    /**
     * 普通参数校验、省略ResultVO返回
     *
     * @param id
     * @return
     */
    @GetMapping("get")
    public TestVO get(@NotNull(message = "id不能为空") Integer id) {
        return testService.get(id);
    }

    /**
     * 返回信息不做统一包装处理
     *
     * @param id
     * @return
     */
    @GetMapping("get2")
    @NotControllerResponseAdvice
    public TestVO get2(@NotNull(message = "id不能为空") Integer id) {
        return testService.get(id);
    }

    /**
     * 字典转换
     *
     * @param
     * @return
     */
    @GetMapping("get4")
    public TestDTO get4() {
        return testService.get4();
    }

    /**
     * 字典转换
     *
     * @param
     * @return
     */
    @GetMapping("get5")
    public List<TestDTO> get5() {
        return testService.get5();
    }


}
