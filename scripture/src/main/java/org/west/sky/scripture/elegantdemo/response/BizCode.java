package org.west.sky.scripture.elegantdemo.response;

import lombok.Getter;

/**
 * @author chenghz
 * @date 2022/6/21 10:52
 * @description: 业务返回码
 */
@Getter
public enum BizCode implements StatusCode {

    /**
     * 业务异常
     */
    BIZ_ERROR(2000, "业务异常"),
    PRICE_ERROR(2001, "价格异常");

    private final int code;
    private final String msg;

    BizCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
