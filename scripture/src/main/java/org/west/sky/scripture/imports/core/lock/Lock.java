package org.west.sky.scripture.imports.core.lock;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Lock {

    String key() default "";

    int ttlSecond() default 3600;

    boolean async() default false;

    String message() default "有相同操作正在进行，请勿重复操作";

}
