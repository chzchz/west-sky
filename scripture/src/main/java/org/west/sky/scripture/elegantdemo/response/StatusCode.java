package org.west.sky.scripture.elegantdemo.response;

/**
 * @author chenghz
 * @date 2022/6/21 10:06
 * @description:
 */
public interface StatusCode {
    /**
     * 状态码
     *
     * @return
     */
    int getCode();

    /**
     * 错误信息
     *
     * @return
     */
    String getMsg();
}
