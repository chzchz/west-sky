package org.west.sky.scripture.imports.core.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean;

/**
 * @author chenghz
 * @date 2023/2/10 13:19
 * @description:
 */
@Component
public class WebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        //注册Handler
        // /equipmentWs/业务大类/业务子类/业务唯一标识
        registry.addHandler(textWebSocketHandler(), "/importWs/{business}/{subclass}/{key}")
                //注册Interceptor
                .addInterceptors(new WebSocketHandshakeInterceptor())
                .setAllowedOrigins("*");

    }

    @Bean
    public ServletServerContainerFactoryBean createWebSocketContainer() {
        ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
        //8192*1024 1024*1024*1024
        container.setMaxTextMessageBufferSize(2 * 1024 * 1024);
        container.setMaxBinaryMessageBufferSize(2 * 1024 * 1024);
        container.setAsyncSendTimeout(55000L);
        //心跳
        container.setMaxSessionIdleTimeout(2 * 3600 * 1000L);
        return container;
    }

    public MessageHandler textWebSocketHandler() {
        return new MessageHandler();
    }

    @Bean
    public WebSocketHandshakeInterceptor getWebSocketInterceptor() {
        return new WebSocketHandshakeInterceptor();
    }


}
