package org.west.sky.scripture.imports.biz.service;

import org.west.sky.scripture.imports.biz.vo.StuVO;
import org.west.sky.scripture.imports.core.base.ImportService;

public interface ImportStuService extends ImportService<StuVO, Object> {
}
