package org.west.sky.scripture.elegantdemo.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.west.sky.scripture.elegantdemo.utils.DictUtil;

/**
 * @author: chz
 * @date: 2023/3/6
 * @description:
 */
@Aspect
@Configuration
public class DictAspect {

    @Pointcut("@annotation(org.west.sky.scripture.elegantdemo.annotation.DictConvert)")
    public void dictCovert() {
    }

    @Around("dictCovert()")
    public Object process(ProceedingJoinPoint joinPoint) throws Throwable {
        //1.执行原方法
        Object proceed = joinPoint.proceed();
        //2.拿到原方法的原返回值 调用parseResult进行字典转换
        Object result = DictUtil.parseResult(proceed);
        DictUtil.clearCache();
        return result;
    }
}
