package org.west.sky.scripture.imports.taskCenter.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.west.sky.scripture.imports.taskCenter.entity.TaskCenterRecord;
import org.west.sky.scripture.imports.taskCenter.mapper.TaskCenterRecordMapper;
import org.west.sky.scripture.imports.taskCenter.service.TaskCenterRecordService;


/**
 * 任务中心(TaskCenter)表服务实现类
 *
 * @author chz
 * @since 2023-04-14 14:06:24
 */
@Slf4j
@Service("taskCenterRecordService")
public class TaskCenterRecordServiceImpl extends ServiceImpl<TaskCenterRecordMapper, TaskCenterRecord> implements TaskCenterRecordService {
}

