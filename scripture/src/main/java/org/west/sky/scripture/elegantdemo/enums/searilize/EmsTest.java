//package org.west.sky.scripture.elegantdemo.enums.searilize;
//
//
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import com.baomidou.mybatisplus.extension.activerecord.Model;
//import com.rn.erp.ems.core.config.UniversalEnumHandler;
//import com.rn.erp.ems.core.enums.SexEnum;
//
//import java.io.Serializable;
//
///**
// * (EmsTest)表实体类
// *
// * @author makejava
// * @since 2023-03-17 15:59:13
// */
//@SuppressWarnings("serial")
//@TableName(value = "ems_test", autoResultMap = true)
//public class EmsTest extends Model<EmsTest> {
//    @TableId(type = IdType.AUTO)
//    private Integer id;
//    private String name;
//
//    @TableField(typeHandler = UniversalEnumHandler.class)
//    private SexEnum sex;
//
//
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public SexEnum getSex() {
//        return sex;
//    }
//
//    public void setSex(SexEnum sex) {
//        this.sex = sex;
//    }
//
//    /**
//     * 获取主键值
//     *
//     * @return 主键值
//     */
//    @Override
//    protected Serializable pkVal() {
//        return this.id;
//    }
//}
//
