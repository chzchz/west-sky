package org.west.sky.scripture.imports.core.observer;


import org.west.sky.scripture.imports.core.base.ImportDataExecuteResult;

import java.util.Map;

/**
 * @author: chz
 * @date: 2023/4/23
 * @description: excel上传观察者
 */
public interface ImportObserver {

    /**
     * excel 执行进度通知
     *
     * @param taskId   任务id
     * @param taskInfo 任务信息
     * @param result   任务执行进度
     */
    void update(String taskId, Map<String, Object> taskInfo, ImportDataExecuteResult result);
}
