package org.west.sky.scripture.imports.core.base;

import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface ImportService<T, E> {

    /**
     * 为了测试导入效果，一次只导入一条数据
     */
    int BATCH_SIZE = 1;

    /**
     * 是否判重，针对excel文件内数据
     *
     * @return
     */
    default boolean verifyDuplicate() {
        return false;
    }

    /**
     * 检查数据
     */
    void checkData(List<T> data, E params);

    /**
     * 保存数据
     */
    void saveData(List<T> data, E params);

    default Collection<List<T>> partition(List<T> data) {
        return Lists.partition(data, BATCH_SIZE);
    }

    /**
     * 构造任务中心参数
     */
    Map<String, Object> getTaskInfo();
}
