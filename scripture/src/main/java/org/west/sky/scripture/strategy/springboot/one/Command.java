package org.west.sky.scripture.strategy.springboot.one;

/**
 * @author chenghz
 * @date 2022/6/17 9:25
 * @description:
 */
public interface Command {

    /**
     * 命令类型
     *
     * @return
     */
    String operatorType();

    /**
     * 执行
     *
     * @param a
     * @param b
     * @return
     */
    Integer execute(int a, int b);
}
