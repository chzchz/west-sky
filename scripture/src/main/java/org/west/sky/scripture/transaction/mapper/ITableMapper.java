package org.west.sky.scripture.transaction.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.west.sky.scripture.transaction.model.TableEntity;

/**
 * @author chenghz
 * @date 2022/8/26 13:58
 * @description:
 */
@Mapper
public interface ITableMapper {

    @Insert("INSERT INTO tablea(id, name) VALUES(#{id}, #{name})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertTableA(TableEntity tableEntity);

    @Insert("INSERT INTO tableb(id, name) VALUES(#{id}, #{name})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertTableB(TableEntity tableEntity);
}
