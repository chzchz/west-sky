package org.west.sky.scripture.imports.taskCenter.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 任务中心(TaskCenter)表实体类
 *
 * @author chz
 * @since 2023-04-14 14:06:18
 */
@Data
@TableName("task_center_record")
public class TaskCenterRecord extends Model<TaskCenterRecord> {
    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 任务id
     */
    private String taskId;
    /**
     * 当前已处理数据
     */
    private Integer totalNum;
    /**
     * 成功条数
     */
    private Integer successNum;
    /**
     * 失败条数
     */
    private Integer failNum;
    /**
     * 执行状态(0:进行中1:已结束)
     */
    private Integer executeStatus;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 文件路径
     */
    private String filePath;
    /**
     * 原始文件路径
     */
    private String oriFilePath;
    /**
     * 结束时间
     */
    private LocalDateTime finishTime;

}

