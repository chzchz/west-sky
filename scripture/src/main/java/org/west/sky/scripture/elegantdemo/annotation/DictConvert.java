package org.west.sky.scripture.elegantdemo.annotation;

import java.lang.annotation.*;

/**
 * 数据字典转换
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DictConvert {
}
