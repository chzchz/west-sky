package org.west.sky.scripture.ratelimiter;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;

import java.time.LocalTime;
import java.util.Collections;

/**
 * @author chz
 * @date 2022/3/21
 * @description: 利用redis incr命令实现固定窗口限流，key放的是当前秒数
 */
@SpringBootTest
public class RedisFixedWindowRateLimiter {

    private static String KEY_PREFIX = "limiter_";
    private static String QPS = "3";
    private static String EXPIRE_TIME = "100";

    private StringRedisTemplate redisTemplate;

    @Autowired
    public RedisFixedWindowRateLimiter(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public boolean acquire(String key) {
        //当前秒数作为key
        key = KEY_PREFIX + key + System.currentTimeMillis() / 1000;
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
        redisScript.setResultType(Long.class);
        //读取resources目录下的lua文件
        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("redisLua/fixedWindow.lua")));
        Long aLong = redisTemplate.execute(redisScript, Collections.singletonList(key), QPS, EXPIRE_TIME);
        return aLong != null && aLong == 1;
    }

    @Test
    public void test() throws InterruptedException {
        for (int i = 0; i < 15; i++) {
            Thread.sleep(200);
            if (this.acquire("user")) {
                System.out.println(LocalTime.now() + " 做点什么");
            } else {
                System.out.println(LocalTime.now() + " 被限流了");
            }
        }
    }
}
