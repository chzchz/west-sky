package org.west.sky.scripture.strategy.tradition;

/**
 * @author chenghz
 * @date 2022/6/17 9:16
 * @description:
 */
public class AddOperation implements Operation {

    @Override
    public int calculate(int a, int b) {
        return a + b;
    }
}
