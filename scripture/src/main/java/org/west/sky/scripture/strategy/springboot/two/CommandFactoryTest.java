package org.west.sky.scripture.strategy.springboot.two;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author chenghz
 * @date 2022/6/17 14:25
 * @description:
 */
@SpringBootTest
public class CommandFactoryTest {

    @Autowired
    private CommandFactory commandFactory;

    @Test
    public void test() {
        int result = commandFactory.calculate("multiCommand", 400, 2);
        System.out.println("result:" + result);
    }
}
