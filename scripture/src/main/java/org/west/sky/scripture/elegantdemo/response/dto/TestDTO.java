package org.west.sky.scripture.elegantdemo.response.dto;

import org.west.sky.scripture.elegantdemo.annotation.Dict;

/**
 * @author: chz
 * @date: 2023/3/6
 * @description:
 */
public class TestDTO {
    /**
     * 年龄
     */
    private String name;
    /**
     * 性别
     */
    @Dict(dictCode = "sex")
//    @Dict(isLocal = true,dictVal = "1：男生；2：女生；3：未知")
    private String sex;

    private String sexValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSexValue() {
        return sexValue;
    }

    public void setSexValue(String sexValue) {
        this.sexValue = sexValue;
    }
}
