package org.west.sky.scripture.elegantdemo.enums.searilize;//package com.rn.erp.ems.core.config;
//
//import com.fasterxml.classmate.util.ClassKey;
//import com.fasterxml.jackson.core.JsonGenerator;
//import com.fasterxml.jackson.core.JsonToken;
//import com.fasterxml.jackson.core.type.WritableTypeId;
//import com.fasterxml.jackson.databind.*;
//import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
//import com.fasterxml.jackson.databind.module.SimpleDeserializers;
//import com.fasterxml.jackson.databind.module.SimpleModule;
//import com.rn.erp.ems.core.enums.BaseEnum;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.http.converter.StringHttpMessageConverter;
//import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.util.List;
//
///**
// * @author: chz
// * @date: 2023/3/17
// * @description:
// */
//@Configuration
//@Slf4j
//public class WebMvcConfig implements WebMvcConfigurer {
//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter(StandardCharsets.UTF_8);
//        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
//        converter.setObjectMapper(objectMapperForWebConvert());
//        converters.add(0, stringHttpMessageConverter);
//        converters.add(0, converter);
//    }
//
//
//    public ObjectMapper objectMapperForWebConvert() {
//        ObjectMapper om = new ObjectMapper();
//        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//        SimpleModule sm = new SimpleModule();
//        //自定义查找规则
//        sm.setDeserializers(new SimpleDeserializers() {
//            @Override
//            public JsonDeserializer<?> findEnumDeserializer(Class<?> type, DeserializationConfig config,
//                                                            BeanDescription beanDesc) throws JsonMappingException {
//                JsonDeserializer<?> enumDeserializer = super.findEnumDeserializer(type, config, beanDesc);
//                if (enumDeserializer != null) {
//                    return enumDeserializer;
//                }
//                //遍历枚举实现的接口, 查找反序列化器
//                for (Class<?> typeInterface : type.getInterfaces()) {
//                    enumDeserializer = this._classMappings.get(new ClassKey(typeInterface));
//                    if (enumDeserializer != null) {
//                        return enumDeserializer;
//                    }
//                }
//                return null;
//            }
//        });
//        sm.addDeserializer(BaseEnum.class, new TypeEnumDeserializer());
//        sm.addSerializer(BaseEnum.class, new JsonSerializer<BaseEnum>() {
//
//            @Override
//            public void serialize(BaseEnum baseEnum, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
//                jsonGenerator.writeStartObject();
//                jsonGenerator.writeNumberField("code", baseEnum.getCode());
//                jsonGenerator.writeStringField("value", baseEnum.getValue());
//                jsonGenerator.writeEndObject();
//            }
//
//            @Override
//            public void serializeWithType(BaseEnum value, JsonGenerator gen, SerializerProvider serializers,
//                                          TypeSerializer typeSer) throws IOException {
//                WritableTypeId typeIdDef = typeSer.writeTypePrefix(gen, typeSer.typeId(value, JsonToken.VALUE_STRING));
//                serialize(value, gen, serializers);
//                typeSer.writeTypeSuffix(gen, typeIdDef);
//            }
//        });
//        om.registerModule(sm);
//        return om;
//    }
//
//}
